import Tkinter as tk  # for Python 2.x version
import tkFont
import os
# import rospy
# from std_msgs.msg import String

def lid_open_cmd():
    print("lid open")
    os.system("rostopic pub /stationLidCmd std_msgs/String open --once")

def lid_close_cmd():
    print("lid close")
    os.system("rostopic pub /stationLidCmd std_msgs/String close --once")

def lid_stop_cmd():
    print("lid stop")
    os.system("rostopic pub /stationLidCmd std_msgs/String stop --once")

def lid_open_and_close_cmd(count):
    for i in range(count):
        print(i, 'lid opening and closing')
        os.system("rostopic pub /stationLidCmd std_msgs/String open --once")

def brush_open_cmd():
    print("brush open")
    os.system("rostopic pub /stationBrushCmd std_msgs/String open --once")

def brush_close_cmd():
    print("brush close")
    os.system("rostopic pub /stationBrushCmd std_msgs/String close --once")

def brush_stop_cmd():
    print("brush stop")
    os.system("rostopic pub /stationBrushCmd std_msgs/String stop --once")

def brush_open_and_close_cmd(count):
    for i in range(count):
        #if brush is open then close
        if open_brush ==  'True':
          os.system("rostopic pub /stationBrushCmd std_msgs/String close --once")
        print(i, 'brush opening and closing')
        os.system("rostopic pub /stationBrushCmd std_msgs/String open --once")
        print(os.system("rostopic pub /stationBrushCmd std_msgs/String open --once"))

def cameraLightsOn1():
    print('cameraLightsOn1')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn1 --once")

def cameraLightsOn2():
    print('cameraLightsOn2')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn2 --once")

def cameraLightsOn3():
    print('cameraLightsOn3')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn3 --once")

def cameraLightsOn4():
    print('cameraLightsOn4')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn4 --once")

def cameraLightsOff1():
    print('cameraLightsOff1')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff1 --once")

def cameraLightsOff2():
    print('cameraLightsOff2')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff2 --once")

def cameraLightsOff3():
    print('cameraLightsOff3')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff3 --once")

def cameraLightsOff4():
    print('cameraLightsOff4')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff4 --once")

def cameraLightsOff():
    print('cameraLightsOff')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff --once")

def chargeOn():
    print('chargeOn')
    os.system("rostopic pub /stationCmds std_msgs/String chargeOn --once")

def chargeOff():
    print('chargeOff')
    os.system("rostopic pub /stationCmds std_msgs/String chargeOff --once")

def droneOn():
    print('droneOn')
    os.system("rostopic pub /stationCmds std_msgs/String droneOn --once")

def droneOff():
    print('droneOff')
    os.system("rostopic pub /stationCmds std_msgs/String droneOff --once")

def arucoLightsOn():
    print('arucoLightsOn')
    os.system("rostopic pub /stationCmds std_msgs/String arucoLightsOn --once")

def arucoLightsOff():
    print('arucoLightsOff')
    os.system("rostopic pub /stationCmds std_msgs/String arucoLightsOff --once")

def fanOn():
    print('fanOn')
    os.system("rostopic pub /stationCmds std_msgs/String fanOn --once")

def fanOff():
    print('fanOff')
    os.system("rostopic pub /stationCmds std_msgs/String fanOff --once")

def buzzer1On():
    print('buzzer1On')
    os.system("rostopic pub /stationCmds std_msgs/String buzzer1On --once")

def buzzer1Off():
    print('buzzer1Off')
    os.system("rostopic pub /stationCmds std_msgs/String buzzer1Off --once")

def buzzer2On():
    print('buzzer2On')
    os.system("rostopic pub /stationCmds std_msgs/String buzzer2On --once")

def buzzer2Off():
    print('buzzer2Off')
    os.system("rostopic pub /stationCmds std_msgs/String buzzer2Off --once")

def statusLight1Red():
    print('statusLight1Red')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight1Red --once")

def statusLight1Amber():
    print('statusLight1Amber')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight1Amber --once")

def statusLight1Green():
    print('statusLight1Green')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight1Green --once")

def statusLight2Red():
    print('statusLight2Red')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight2Red --once")

def statusLight2Amber():
    print('statusLight2Amber')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight2Amber --once")

def statusLight2Green():
    print('statusLight2Green')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight2Green --once")

def statusLight3Red():
    print('statusLight3Red')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight3Red --once")

def statusLight3Amber():
    print('statusLight3Amber')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight3Amber --once")

def statusLight3Green():
    print('statusLight3Green')
    os.system("rostopic pub /stationCmds std_msgs/String statusLight3Green --once")

def open_lid_motor_left():
    print('open_lid_motor_left')
    os.system("rostopic pub /stationLidMotorCmd std_msgs/String leftOpen --once")

def close_lid_motor_left():
    print('close_lid_motor_left')
    os.system("rostopic pub /stationLidMotorCmd std_msgs/String leftClose --once")

def stop_lid_motor_left():
    print('stop_lid_motor_left')
    os.system("rostopic pub /stationLidMotorCmd std_msgs/String leftStop --once")

def open_lid_motor_right():
    print('open_lid_motor_right')
    os.system("rostopic pub /stationLidMotorCmd std_msgs/String rightOpen --once")

def close_lid_motor_right():
    print('close_lid_motor_right')
    os.system("rostopic pub /stationLidMotorCmd std_msgs/String rightClose --once")

def stop_lid_motor_right():
    print('stop_lid_motor_right')
    os.system("rostopic pub /stationLidMotorCmd std_msgs/String rightStop --once")

def open_charge_brush_motor():
    print('open_charge_brush_motor')
    os.system("rostopic pub /stationBrushMotorCmd std_msgs/String chargeOpen --once")

def close_charge_brush_motor():
    print('close_charge_brush_motor')
    os.system("rostopic pub /stationBrushMotorCmd std_msgs/String chargeClose --once")

def stop_charge_brush_motor():
    print('stop_charge_brush_motor')
    os.system("rostopic pub /stationBrushMotorCmd std_msgs/String chargeStop --once")

def open_side_brush_motor():
    print('open_side_brush_motor')
    os.system("rostopic pub /stationBrushMotorCmd std_msgs/String sideOpen --once")

def close_side_brush_motor():
    print('close_side_brush_motor')
    os.system("rostopic pub /stationBrushMotorCmd std_msgs/String sideClose --once")

def stop_side_brush_motor():
    print('stop_side_brush_motor')
    os.system("rostopic pub /stationBrushMotorCmd std_msgs/String sideStop --once")

def lid_switch_status_cb(data, button):
    lid_status_array = data.data.split()

    left_open = lid_status_array[4]
    left_close = lid_status_array[6]
    right_open = lid_status_array[8]
    right_close = lid_status_array[10]

    if left_open == 'True':
      button[0]['bg'] = "green"
      print("True left_open")
    elif left_open == 'False':
      button[0]['bg'] = "red"
      print("False left_open")
    else:
      print("Error left_open")

    if left_close == 'True':
      button[1]['bg'] = "green"
      print("True left_close")
    elif left_close == 'False':
      button[1]['bg'] = "red"
      print("False left_close")
    else:
      print("Error left_close")

    if right_open == 'True':
      button[2]['bg'] = "green"
      print("True right_open")
    elif right_open == 'False':
      button[2]['bg'] = "red"
      print("False right_open")
    else:
      print("Error right_open")

    if right_close == 'True':
      button[3]['bg'] = "green"
      print("True right_close")
    elif right_close == 'False':
      button[3] ['bg'] = "red"
      print("False right_close")
    else:
      print("Error right_close")

def lid_switch_control():
    lid_switch_controls_window = tk.Toplevel(root)
    lid_switch_controls_window.geometry("1920x1080")
    lid_switch_controls_window.title("Lid Limit Switch Controls")
    lid_switch_controls_window.rowconfigure((1, 2), weight=1)
    lid_switch_controls_window.columnconfigure((0, 2), weight=1)

    tk.Button(lid_switch_controls_window, text="Back", fg="red", font=FONT, command=lid_switch_controls_window.destroy).grid(row=0, column=0, columnspan=3, sticky='EWNS')
    left_open = tk.Button(lid_switch_controls_window, disabledforeground="black", bg='red', text='Left Open', font=FONT, state="disabled")
    left_close = tk.Button(lid_switch_controls_window, disabledforeground="black", bg='red', text='Left Close', font=FONT, state="disabled")
    right_open = tk.Button(lid_switch_controls_window, disabledforeground="black", bg='red', text='Right Open', font=FONT, state="disabled")
    right_close = tk.Button(lid_switch_controls_window, disabledforeground="black", bg='red', text='Right Close', font=FONT, state="disabled")

    buttons = [left_open, left_close, right_open, right_close]
    # rospy.Subscriber("stationLidSwitchStatus", String, lid_switch_status_cb, buttons)

    left_open.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    left_close.grid(row=1, column=1, columnspan=2, sticky='EWNS')
    right_open.grid(row=2, column=0, columnspan=1, sticky='EWNS')
    right_close.grid(row=2, column=1, columnspan=2, sticky='EWNS')

def brush_switch_status_cb(data, button):
    brush_status_array = data.data.split()

    charge_open = brush_status_array[4]
    charge_close = brush_status_array[6]
    side_open = brush_status_array[8]
    side_close = brush_status_array[10]

    if charge_open == 'True':
      button[0]['bg'] = "green"
      print("True left_open")
    elif charge_open == 'False':
      button[0]['bg'] = "red"
      print("False left_open")
    else:
      print("Error left_open")

    if charge_close == 'True':
      button[1]['bg'] = "green"
      print("True left_close")
    elif charge_close == 'False':
      button[1]['bg'] = "red"
      print("False left_close")
    else:
      print("Error left_close")

    if side_open == 'True':
      button[2]['bg'] = "green"
      print("True right_open")
    elif side_open == 'False':
      button[2]['bg'] = "red"
      print("False right_open")
    else:
      print("Error right_open")

    if side_close == 'True':
      button[3]['bg'] = "green"
      print("True right_close")
    elif side_close == 'False':
      button[3] ['bg'] = "red"
      print("False right_close")
    else:
      print("Error right_close")

def landing_pad_switch_control():
    landing_pad_controls_window = tk.Toplevel(root)
    landing_pad_controls_window.geometry("1920x1080")
    landing_pad_controls_window.title("Landing Pad Limit Switch Controls")
    landing_pad_controls_window.rowconfigure((1, 2), weight=1)
    landing_pad_controls_window.columnconfigure((0, 2), weight=1)

    tk.Button(landing_pad_controls_window, text="Back", fg="red", font=FONT, command=landing_pad_controls_window.destroy).grid(row=0, column=0, columnspan=3, sticky='EWNS')
    charge_open = tk.Button(landing_pad_controls_window, disabledforeground="black", bg='red', text='Charge Open', font=FONT, state="disabled")
    charge_close = tk.Button(landing_pad_controls_window, disabledforeground="black", bg='red', text='Charge Close', font=FONT, state="disabled")
    side_open = tk.Button(landing_pad_controls_window, disabledforeground="black", bg='red', text='Side Open', font=FONT, state="disabled")
    side_close = tk.Button(landing_pad_controls_window, disabledforeground="black", bg='red', text='Side Close', font=FONT, state="disabled")

    buttons = [charge_open, charge_close, side_open, side_close]
    # rospy.Subscriber("stationBrushSwitchStatus", String, brush_switch_status_cb, buttons)

    charge_open.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    charge_close.grid(row=1, column=1, columnspan=2, sticky='EWNS')
    side_open.grid(row=2, column=0, columnspan=1, sticky='EWNS')
    side_close.grid(row=2, column=1, columnspan=2, sticky='EWNS')

def motor_lid_control():
    lid_motor_controls_window = tk.Toplevel(root)
    lid_motor_controls_window.geometry("1920x1080")
    lid_motor_controls_window.title("Lid Motor Controls")
    lid_motor_controls_window.rowconfigure((1, 2), weight=1)
    lid_motor_controls_window.columnconfigure((0, 2), weight=1)

    tk.Button(lid_motor_controls_window, text="Back", fg="red", font=FONT, command=lid_motor_controls_window.destroy).grid(row=0, column=0, columnspan=3, sticky='EWNS')
    tk.Button(lid_motor_controls_window, text='On', font=FONT, command=open_lid_motor_left).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(lid_motor_controls_window, text='Off', font=FONT, command=close_lid_motor_left).grid(row=1, column=1, columnspan=2, sticky='EWNS')
    tk.Button(lid_motor_controls_window, text='Stop', font=FONT, command=stop_lid_motor_left).grid(row=1, column=2, columnspan=2, sticky='EWNS')
    tk.Button(lid_motor_controls_window, text='On', font=FONT, command=open_lid_motor_right).grid(row=2, column=0, columnspan=1, sticky='EWNS')
    tk.Button(lid_motor_controls_window, text='Off', font=FONT, command=close_lid_motor_right).grid(row=2, column=1, columnspan=1, sticky='EWNS')
    tk.Button(lid_motor_controls_window, text='Stop', font=FONT, command=stop_lid_motor_right).grid(row=2, column=2, columnspan=1, sticky='EWNS')

def motor_brushes_control():
    brush_motor_controls_window = tk.Toplevel(root)
    brush_motor_controls_window.geometry("1920x1080")
    brush_motor_controls_window.title("Brush Motor Controls")
    brush_motor_controls_window.rowconfigure((1, 2), weight=1)
    brush_motor_controls_window.columnconfigure((0, 2), weight=1)

    tk.Button(brush_motor_controls_window, text="Back", fg="red", font=FONT, command=brush_motor_controls_window.destroy).grid(row=0, column=0, columnspan=3, sticky='EWNS')
    tk.Button(brush_motor_controls_window, text='On', font=FONT, command=open_charge_brush_motor).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(brush_motor_controls_window, text='Off', font=FONT, command=close_charge_brush_motor).grid(row=1, column=1, columnspan=1, sticky='EWNS')
    tk.Button(brush_motor_controls_window, text='Stop', font=FONT, command=stop_charge_brush_motor).grid(row=1, column=2, columnspan=1, sticky='EWNS')
    tk.Button(brush_motor_controls_window, text='On', font=FONT, command=open_side_brush_motor).grid(row=2, column=0, columnspan=1, sticky='EWNS')
    tk.Button(brush_motor_controls_window, text='Off', font=FONT, command=close_side_brush_motor).grid(row=2, column=1, columnspan=1, sticky='EWNS')
    tk.Button(brush_motor_controls_window, text='Stop', font=FONT, command=stop_side_brush_motor).grid(row=2, column=2, columnspan=1, sticky='EWNS')

def station_lights():
    station_lights_controls_window = tk.Toplevel(root)
    station_lights_controls_window.geometry("1920x1080")
    station_lights_controls_window.title("Station Light Controls")
    station_lights_controls_window.rowconfigure((1, 2), weight=1)
    station_lights_controls_window.columnconfigure((0, 2), weight=1)

    # buttons = [lid_switch_left_open, lid_switch_left_close, lid_switch_right_open, lid_switch_right_close]
    # rospy.Subscriber("stationStatusMotorDriver", String, motor_status_cb) # Create subscriber

    tk.Button(station_lights_controls_window, text="Back", fg="red", font=FONT, command=station_lights_controls_window.destroy).grid(row=0, column=0, columnspan=3, sticky='EWNS')
    tk.Button(station_lights_controls_window, disabledforeground="black", text='Left Open', font=FONT, state="disabled").grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(station_lights_controls_window, disabledforeground="black", text='Left Close', font=FONT, state="disabled").grid(row=1, column=1, columnspan=2, sticky='EWNS')
    tk.Button(station_lights_controls_window, disabledforeground="black", bext='Right Open', font=FONT, state="disabled").grid(row=2, column=0, columnspan=1, sticky='EWNS')
    tk.Button(station_lights_controls_window, disabledforeground="black", text='Right Close', font=FONT, state="disabled").grid(row=2, column=1, columnspan=2, sticky='EWNS')

def lid_control():
    lid_controls_window = tk.Toplevel(root)
    lid_controls_window.title("Lid Controls")
    lid_controls_window.geometry("1920x1080")
    lid_controls_window.rowconfigure((0, 1), weight=1)
    lid_controls_window.columnconfigure((0, 1, 2, 3), weight=1)

    tk.Button(lid_controls_window, text='Back', fg="red", font=FONT, command=lid_controls_window.destroy).grid(row=0, column=0, columnspan=1, sticky='EWNS')
    tk.Button(lid_controls_window, text='Open', font=FONT, command=lid_open_cmd).grid(row=0, column=1, columnspan=1, sticky='EWNS')
    tk.Button(lid_controls_window, text='Close', font=FONT, command=lid_close_cmd).grid(row=0, column=2, columnspan=1, sticky='EWNS')
    tk.Button(lid_controls_window, text='Stop', font=FONT, command=lid_stop_cmd).grid(row=0, column=3, columnspan=1, sticky='EWNS')
    tk.Button(lid_controls_window, text='5', font=FONT, command=lambda: lid_open_and_close_cmd(5)).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(lid_controls_window, text='10', font=FONT, command=lambda: lid_open_and_close_cmd(10)).grid(row=1, column=1, columnspan=1, sticky='EWNS')
    tk.Button(lid_controls_window, text='20', font=FONT, command=lambda: lid_open_and_close_cmd(20)).grid(row=1, column=2, columnspan=1, sticky='EWNS')
    tk.Button(lid_controls_window, text='50', font=FONT, command=lambda: lid_open_and_close_cmd(50)).grid(row=1, column=3, columnspan=1, sticky='EWNS')

def charge_control():
    charge_controls_window = tk.Toplevel(root)
    charge_controls_window.geometry("1920x1080")
    charge_controls_window.title("Charge Controls")
    charge_controls_window.rowconfigure((0, 1), weight=1)
    charge_controls_window.columnconfigure((0, 1), weight=1)

    tk.Button(charge_controls_window, text="Back", fg="red", font=FONT, command=charge_controls_window.destroy).grid(row=0, column=0, columnspan=3, sticky='EWNS')
    tk.Button(charge_controls_window, text='On', font=FONT, command=chargeOn).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(charge_controls_window, text='Off', font=FONT, command=chargeOff).grid(row=1, column=1, columnspan=2, sticky='EWNS')

def drone_power_control():
    drone_power_controls_window = tk.Toplevel(root)
    drone_power_controls_window.geometry("1920x1080")
    drone_power_controls_window.title("Drone Power Controls")
    drone_power_controls_window.rowconfigure((0, 1), weight=1)
    drone_power_controls_window.columnconfigure((0, 1), weight=1)

    tk.Button(drone_power_controls_window, text="Back", fg="red", font=FONT, command=drone_power_controls_window.destroy).grid(row=0, column=0, columnspan=3, sticky='EWNS')
    tk.Button(drone_power_controls_window, text='On', font=FONT, command=droneOn).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(drone_power_controls_window, text='Off', font=FONT, command=droneOff).grid(row=1, column=1, columnspan=2, sticky='EWNS')

def buzzer_control():
    drone_power_controls_window = tk.Toplevel(root)
    drone_power_controls_window.geometry("1920x1080")
    drone_power_controls_window.title("Buzzer Controls")
    drone_power_controls_window.rowconfigure((1, 2), weight=1)
    drone_power_controls_window.columnconfigure((1, 2), weight=1)

    tk.Button(drone_power_controls_window, text="Back", fg="red", font=FONT, command=drone_power_controls_window.destroy).grid(row=0, column=0, columnspan=3, sticky='EWNS')
    tk.Button(drone_power_controls_window, text='1', font=FONT, disabledforeground="black",  state="disabled").grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(drone_power_controls_window, text='On', font=FONT, command=buzzer1On).grid(row=1, column=1, columnspan=1, sticky='EWNS')
    tk.Button(drone_power_controls_window, text='Off', font=FONT, command=buzzer2Off).grid(row=1, column=2, columnspan=1, sticky='EWNS')
    tk.Button(drone_power_controls_window, text='2', font=FONT, disabledforeground="black",  state="disabled").grid(row=2, column=0, columnspan=1, sticky='EWNS')
    tk.Button(drone_power_controls_window, text='On', font=FONT, command=buzzer2On).grid(row=2, column=1, columnspan=1, sticky='EWNS')
    tk.Button(drone_power_controls_window, text='Off', font=FONT, command=buzzer2Off).grid(row=2, column=2, columnspan=1, sticky='EWNS')

def brushes_control():
    brushes_control_window = tk.Toplevel(root)
    brushes_control_window.title("Brushes Controls")
    brushes_control_window.geometry("1920x1080")
    brushes_control_window.rowconfigure((0, 1), weight=1)
    brushes_control_window.columnconfigure((0, 1, 2, 3), weight=1)

    tk.Button(brushes_control_window, text='Back', fg="red", font=FONT, command=brushes_control_window.destroy).grid(row=0, column=0, columnspan=1, sticky='EWNS')
    tk.Button(brushes_control_window, text='Open', font=FONT, command=brush_open_cmd).grid(row=0, column=1, columnspan=1, sticky='EWNS')
    tk.Button(brushes_control_window, text='Close', font=FONT, command=brush_close_cmd).grid(row=0, column=2, columnspan=1, sticky='EWNS')
    tk.Button(brushes_control_window, text='Stop', font=FONT, command=brush_stop_cmd).grid(row=0, column=3, columnspan=1, sticky='EWNS')
    tk.Button(brushes_control_window, text='5', font=FONT, command=lambda: brush_open_and_close_cmd(5)).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(brushes_control_window, text='10', font=FONT, command=lambda: brush_open_and_close_cmd(10)).grid(row=1, column=1, columnspan=1, sticky='EWNS')
    tk.Button(brushes_control_window, text='20', font=FONT, command=lambda: brush_open_and_close_cmd(20)).grid(row=1, column=2, columnspan=1, sticky='EWNS')
    tk.Button(brushes_control_window, text='50', font=FONT, command=lambda: brush_open_and_close_cmd(50)).grid(row=1, column=3, columnspan=1, sticky='EWNS')

def light_control():
    light_control_window = tk.Toplevel(root)
    light_control_window.title("Light Controls")
    light_control_window.geometry("1920x1080")
    light_control_window.rowconfigure((1, 2), weight=1)
    light_control_window.columnconfigure((0, 1, 2, 3, 4), weight=1)

    tk.Button(light_control_window, text='Back', fg="red", font=FONT, command=light_control_window.destroy).grid(row=0, column=0, columnspan=5, sticky='EWNS')
    tk.Button(light_control_window, text='On', font=FONT, command=arucoLightsOn).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='1', font=FONT, command=cameraLightsOn1).grid(row=1, column=1, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='2', font=FONT, command=cameraLightsOn2).grid(row=1, column=2, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='3', font=FONT, command=cameraLightsOn3).grid(row=1, column=3, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='4', font=FONT, command=cameraLightsOn4).grid(row=1, column=4, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='Off', font=FONT, command=arucoLightsOff).grid(row=2, column=0, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='1', font=FONT, command=cameraLightsOff1).grid(row=2, column=1, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='2', font=FONT, command=cameraLightsOff2).grid(row=2, column=2, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='3', font=FONT, command=cameraLightsOff3).grid(row=2, column=3, columnspan=1, sticky='EWNS')
    tk.Button(light_control_window, text='4', font=FONT, command=cameraLightsOff4).grid(row=2, column=4, columnspan=1, sticky='EWNS')

def landing_pad_commands():
    landing_pad_commands_window = tk.Toplevel(root)
    landing_pad_commands_window.title("Landing Pad Commands")
    landing_pad_commands_window.geometry("1920x1080")
    landing_pad_commands_window.rowconfigure((0, 1), weight=1)
    landing_pad_commands_window.columnconfigure((0, 2), weight=1)

    tk.Button(landing_pad_commands_window, text='Back', fg="red", font=FONT, command=landing_pad_commands_window.destroy).grid(row=0, column=0, columnspan=1, sticky='EWNS')
    tk.Button(landing_pad_commands_window, text='Brushes', font=FONT, command=brushes_control).grid(row=0, column=1, columnspan=1, sticky='EWNS')
    tk.Button(landing_pad_commands_window, text='Charge', font=FONT, command=charge_control).grid(row=0, column=2, columnspan=1, sticky='EWNS')
    tk.Button(landing_pad_commands_window, text='Drone Power', font=FONT, command=drone_power_control).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(landing_pad_commands_window, text='Motor', font=FONT, command=motor_brushes_control).grid(row=1, column=1, columnspan=1, sticky='EWNS')
    tk.Button(landing_pad_commands_window, text='Limit Switches', font=FONT, command=landing_pad_switch_control).grid(row=1, column=2, columnspan=1, sticky='EWNS')

def lid_commands():
    lid_commands_window = tk.Toplevel(root)
    lid_commands_window.title("Lid Commands")
    lid_commands_window.geometry("1920x1080")
    lid_commands_window.rowconfigure((0, 1), weight=1)
    lid_commands_window.columnconfigure((0, 2), weight=1)

    tk.Button(lid_commands_window, text='Back', fg="red",font=FONT, command=lid_commands_window.destroy).grid(row=0, column=0, columnspan=1, sticky='EWNS')
    tk.Button(lid_commands_window, text='Lid',font=FONT, command=lid_control).grid(row=0, column=1, columnspan=1, sticky='EWNS')
    tk.Button(lid_commands_window, text='Limit Switches', font=FONT, command=lid_switch_control).grid(row=0, column=2, columnspan=1, sticky='EWNS')
    tk.Button(lid_commands_window, text='Motor', font=FONT, command=motor_lid_control).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(lid_commands_window, text='Aruco Lights', font=FONT, command=light_control).grid(row=1, column=1, columnspan=1, sticky='EWNS')
    tk.Button(lid_commands_window, text='Camera', font=FONT).grid(row=1, column=2, columnspan=1, sticky='EWNS')

def station_commands():
    station_commands_window = tk.Toplevel(root)
    station_commands_window.title("Station Commands")
    station_commands_window.geometry("1920x1080")
    station_commands_window.rowconfigure((0, 1), weight=1)
    station_commands_window.columnconfigure((0, 2), weight=1)

    tk.Button(station_commands_window, text='Back', fg="red",font=FONT, command=station_commands_window.destroy).grid(row=0, column=0, columnspan=1, sticky='EWNS')
    tk.Button(station_commands_window, text='Buzzer',font=FONT, command=buzzer_control).grid(row=0, column=1, columnspan=1, sticky='EWNS')
    tk.Button(station_commands_window, text='Lights', font=FONT).grid(row=0, column=2, columnspan=1, sticky='EWNS')

def home_page():
    root.rowconfigure((0, 1), weight=1)
    root.columnconfigure((0, 2), weight=1)
    root.title("Station Control Application")

    tk.Button(text='QUIT', fg="red", font=FONT, command=root.destroy).grid(row=0, column=0, columnspan=1, sticky='EWNS')
    tk.Button(text='Full station test', font=FONT).grid(row=0, column=1, columnspan=2, sticky='EWNS')
    tk.Button(text='Lid', font=FONT, command=lid_commands).grid(row=1, column=0, columnspan=1, sticky='EWNS')
    tk.Button(text='Landing Pad', font=FONT, command=landing_pad_commands).grid(row=1, column=1, columnspan=1, sticky='EWNS')
    tk.Button(text='Station', font=FONT, command=station_commands).grid(row=1, column=2, columnspan=1, sticky='EWNS')

root = tk.Tk()
root.geometry("1920x1080")
FONT = tkFont.Font(family='Helvetica', size=70, weight=tkFont.BOLD)

# rospy.init_node('build_console', anonymous=True) # Create ros node

home_page()

root.mainloop()
