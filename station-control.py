#!/usr/bin/env python
import sys
import PySimpleGUI as sg
import os
# import rospy
# from std_msgs.msg import String


theme_dict = {'BACKGROUND': '#2B475D',
              'TEXT': '#FFFFFF',
              'INPUT': '#F2EFE8',
              'TEXT_INPUT': '#000000',
              'SCROLL': '#F2EFE8',
              'BUTTON': ('#000000', '#C2D4D8'),
              'PROGRESS': ('#FFFFFF', '#C7D5E0'),
              'BORDER': 1, 'SLIDER_DEPTH': 0, 'PROGRESS_DEPTH': 0}
orange = '#FF8801'

BORDER_COLOR = '#C7D5E0'
DARK_HEADER_COLOR = '#1B2838'
RED = '#FF0000'
GREEN = '#008000'
BPAD_TOP = ((20, 20), (20, 10))
BPAD_LEFT = ((20, 10), (0, 10))
BPAD_LEFT_INSIDE = (0, 10)
BPAD_RIGHT_INSIDE = (20, 20)
BPAD_RIGHT = ((10, 20), (10, 20))
BPAD_FAR_RIGHT = ((0, 30), (20, 30))

LEFT_LID_OPEN = False
LEFT_LID_CLOSE = False


def lid_open():
    print("lid open test")
    # os.system("rostopic pub /stationLidCmd std_msgs/String open --once")


def lid_close():
    print("lid close test")
    # os.system("rostopic pub /stationLidCmd std_msgs/String close --once")


def lid_stop():
    print("lid stop test")
    # os.system("rostopic pub /stationLidCmd std_msgs/String stop --once")

def arucoLightsOn():
    print('arucoLightsOn')
    # os.system("rostopic pub /stationCmds std_msgs/String arucoLightsOn --once")

def arucoLightsOff():
    print('arucoLightsOff')
    # os.system("rostopic pub /stationCmds std_msgs/String arucoLightsOff --once")

def lid():
    sg.LOOK_AND_FEEL_TABLE['Lid'] = theme_dict
    sg.theme('Lid')

    print(LEFT_LID_OPEN, 'LEFT_LID_OPEN')

    # LEFT_LID_OPEN = False
    # LEFT_LID_CLOSE = False
    RIGHT_LID_OPEN = False
    RIGHT_LID_CLOSE = False

    if LEFT_LID_OPEN:
        left_open_colour = GREEN
    else:
        left_open_colour = RED

    if LEFT_LID_CLOSE:
        left_close_colour = GREEN
    else:
        left_close_colour = RED

    if RIGHT_LID_OPEN:
        right_open_colour = GREEN
        right_close_colour = RED
    else:
        right_open_colour = RED
        right_close_colour = GREEN

    top_banner = [[sg.Text('Lid Commands' + ' '*64,
                           font='Any 20', background_color=DARK_HEADER_COLOR)]]
    status = [[sg.Text('<Display Status Here>' + ' '*64,
                       font='Any 20', background_color=DARK_HEADER_COLOR)]]

    left_open = [[sg.Text('Left Open', font='Any 20')],
                 [sg.Button('Open', key='left_lid_open', font='Any 30', button_color=('white', left_open_colour))]]

    left_close = [[sg.Text('Left Close', font='Any 20')],
                  [sg.Button('Close', key='left_lid_close', font='Any 30', button_color=('white', left_close_colour))]]

    right_open = [[sg.Text('Right Open', font='Any 20')],
                  [sg.Button('Open', key='right_lid_open', font='Any 30', button_color=('white', RED))]]

    right_close = [[sg.Text('Right Close', font='Any 20')],
                   [sg.Button('Close', key='right_lid_close', font='Any 30', button_color=('white', RED))]]

    commands = list_commands('lids')

    logs = [[sg.Text('Logs', font='Any 20')]]

    layout = [[sg.Column(top_banner, size=(1280, 50), pad=(0, 0), background_color=DARK_HEADER_COLOR)],
              [sg.Column(status, size=(1240, 90), pad=BPAD_TOP)],
              [sg.Column([[sg.Column(left_open, size=(220, 120), pad=BPAD_LEFT_INSIDE)],
                          [sg.Column(right_open, size=(220, 120),  pad=BPAD_LEFT_INSIDE)]], pad=BPAD_LEFT, background_color=BORDER_COLOR),
              sg.Column([[sg.Column(left_close, size=(220, 120), pad=BPAD_LEFT_INSIDE)],
                         [sg.Column(right_close, size=(220, 120),  pad=BPAD_LEFT_INSIDE)]], pad=BPAD_RIGHT, background_color=BORDER_COLOR),
              sg.Column(commands, size=(350, 260), pad=BPAD_RIGHT),
              sg.Column(logs, size=(350, 260), pad=BPAD_RIGHT)]]

    return sg.Window('Lid', layout, size=(1280, 480),
                     margins=(0, 0), background_color=BORDER_COLOR, resizable=True, finalize=True)



def aruco_lights():
    sg.LOOK_AND_FEEL_TABLE['Aruco Lights'] = theme_dict
    sg.theme('Aruco Lights')

    top_banner = [[sg.Text('Aruco Lights' + ' '*64,
                           font='Any 20', background_color=DARK_HEADER_COLOR)]]
    status = [[sg.Text('<Display Status Here>' + ' '*64,
                       font='Any 20', background_color=DARK_HEADER_COLOR)]]

    left_open = [[sg.Text('Light 1', font='Any 20')],
                 [sg.Button('On/Off', key='left_lid_open', font='Any 30', button_color=('white', RED))]]

    left_close = [[sg.Text('Light 2', font='Any 20')],
                  [sg.Button('On/Off', key='left_lid_close', font='Any 30', button_color=('white', RED))]]

    right_open = [[sg.Text('Light 3', font='Any 20')],
                  [sg.Button('On/Off', key='right_lid_open', font='Any 30', button_color=('white', RED))]]

    right_close = [[sg.Text('Light 4', font='Any 20')],
                   [sg.Button('On/Off', key='right_lid_close', font='Any 30', button_color=('white', RED))]]

    commands = list_commands('arucoLights')

    logs = [[sg.Text('Logs', font='Any 20')]]

    layout = [[sg.Column(top_banner, size=(1280, 50), pad=(0, 0), background_color=DARK_HEADER_COLOR)],
              [sg.Column(status, size=(1240, 90), pad=BPAD_TOP)],
              [sg.Column([[sg.Column(left_open, size=(220, 120), pad=BPAD_LEFT_INSIDE)],
                          [sg.Column(right_open, size=(220, 120),  pad=BPAD_LEFT_INSIDE)]], pad=BPAD_LEFT, background_color=BORDER_COLOR),
              sg.Column([[sg.Column(left_close, size=(220, 120), pad=BPAD_LEFT_INSIDE)],
                         [sg.Column(right_close, size=(220, 120),  pad=BPAD_LEFT_INSIDE)]], pad=BPAD_RIGHT, background_color=BORDER_COLOR),
              sg.Column(commands, size=(350, 260), pad=BPAD_RIGHT),
              sg.Column(logs, size=(350, 260), pad=BPAD_RIGHT)]]

    return sg.Window('Aruco Lights', layout, size=(1280, 480),
                     margins=(0, 0), background_color=BORDER_COLOR, resizable=True, finalize=True)



def camera():
    sg.LOOK_AND_FEEL_TABLE['Camera'] = theme_dict
    sg.theme('Camera')

    top_banner = [[sg.Text('Camera' + ' '*64,
                           font='Any 20', background_color=DARK_HEADER_COLOR)]]
    status = [[sg.Text('<Display Status Here>' + ' '*64,
                       font='Any 20', background_color=DARK_HEADER_COLOR)]]

    left_open = [[sg.Text('Camera 1', font='Any 20')],
                 [sg.Button('On/Off', key='left_lid_open', font='Any 30', button_color=('white', RED))]]

    left_close = [[sg.Text('Camera 2', font='Any 20')],
                  [sg.Button('On/Off', key='left_lid_close', font='Any 30', button_color=('white', RED))]]

    right_open = [[sg.Text('Camera 3', font='Any 20')],
                  [sg.Button('On/Off', key='right_lid_open', font='Any 30', button_color=('white', RED))]]

    right_close = [[sg.Text('Camera 4', font='Any 20')],
                   [sg.Button('On/Off', key='right_lid_close', font='Any 30', button_color=('white', RED))]]

    commands = list_commands('camera')

    logs = [[sg.Text('Logs', font='Any 20')]]

    layout = [[sg.Column(top_banner, size=(1280, 50), pad=(0, 0), background_color=DARK_HEADER_COLOR)],
              [sg.Column(status, size=(1240, 90), pad=BPAD_TOP)],
              [sg.Column([[sg.Column(left_open, size=(220, 120), pad=BPAD_LEFT_INSIDE)],
                          [sg.Column(right_open, size=(220, 120),  pad=BPAD_LEFT_INSIDE)]], pad=BPAD_LEFT, background_color=BORDER_COLOR),
              sg.Column([[sg.Column(left_close, size=(220, 120), pad=BPAD_LEFT_INSIDE)],
                         [sg.Column(right_close, size=(220, 120),  pad=BPAD_LEFT_INSIDE)]], pad=BPAD_RIGHT, background_color=BORDER_COLOR),
              sg.Column(commands, size=(350, 260), pad=BPAD_RIGHT),
              sg.Column(logs, size=(350, 260), pad=BPAD_RIGHT)]]

    return sg.Window('Camera', layout, size=(1280, 480),
                     margins=(0, 0), background_color=BORDER_COLOR, resizable=True, finalize=True)


def list_commands(command):
    if command == 'lids':
        commands = [[sg.Text('Commands', font='Any 20')],
                    [sg.Button('Open', key=lid_open, font='Any 25'),
                     sg.Button('Close', key=lid_close, font='Any 25'),
                     sg.Button('Stop', key=lid_stop, font='Any 25')],
                    [sg.Button('10', key=lambda: lid_open_and_close(10), font='Any 40'),
                     sg.Button('15', key=lambda: lid_open_and_close(
                         15), font='Any 40'),
                     sg.Button('20', key=lambda: lid_open_and_close(20), font='Any 40')]]

    if command == 'arucoLights':
        commands = [[sg.Text('Commands', font='Any 20')],
                    [sg.Button('On', key=arucoLightsOn, font='Any 35'),
                     sg.Button('Off', key=arucoLightsOff, font='Any 35')],
                    [sg.Button('10', key=lambda: arucoLightsOnOff(10), font='Any 40'),
                     sg.Button('15', key=lambda: arucoLightsOnOff(
                         15), font='Any 40'),
                     sg.Button('20', key=lambda: arucoLightsOnOff(20), font='Any 40')]]


    if command == 'camera':
        commands = [[sg.Text('Commands', font='Any 20')],
                    [sg.Button('On', key=arucoLightsOn, font='Any 35'),
                     sg.Button('Off', key=arucoLightsOff, font='Any 35')],
                    [sg.Button('10', key=lambda: arucoLightsOnOff(10), font='Any 40'),
                     sg.Button('15', key=lambda: arucoLightsOnOff(
                         15), font='Any 40'),
                     sg.Button('20', key=lambda: arucoLightsOnOff(20), font='Any 40')]]

    return commands

def arucoLightsOnOff(number):
    for i in range(number):
        arucoLightsOn()
        arucoLightsOff()
        print(i, 'arucoLightsOn and arucoLightsOff')

def lid_open_and_close(number):
    for i in range(number):
        lid_open()
        lid_close()
        print(i, 'opening and closing')


def generate_windows(name, buttons):
    sg.LOOK_AND_FEEL_TABLE['Dashboard'] = theme_dict
    sg.theme('Dashboard')

    top_banner = [[sg.Text(name + ' '*64, font='Any 20', background_color=DARK_HEADER_COLOR),
                  sg.Text('Date and Time', font='Any 20', background_color=DARK_HEADER_COLOR)]]

    menu_bar = []
    for i in range(len(buttons)):
        if buttons[i] == 'Back':
            menu_bar.append(
                sg.Button(buttons[i], font='Any 20', button_color=('white', 'firebrick3')))
        else:
            menu_bar.append(sg.Button(buttons[i], font='Any 20'))

    menu_bar = [menu_bar]

    layout = [[sg.Column(top_banner, size=(1280, 50), pad=(0, 0), background_color=DARK_HEADER_COLOR)],
              [sg.Column(menu_bar, size=(1100, 90), pad=BPAD_TOP)]
              ]

    window = sg.Window(name, layout, size=(1280, 480),
                       margins=(0, 0), background_color=BORDER_COLOR, resizable=True, finalize=True)

    return window


def landing_pad_commands():
    windows = generate_windows('Landing Pad Commands', [
                               'Back', 'Brushes', 'Charges', 'Drone Power', 'Limit Switches', 'Motor'])
    return windows


def lid_commands():
    windows = generate_windows('Lid Commands', [
                               'Back', 'Lid', 'Limit Switches', 'Motor', 'Aruco Lights', 'Camera'])
    return windows


def main():

    window = generate_windows('Station Testing App', [
                              'Full Test', 'Walkthrough', 'Landing Pad Test', 'Lid Test'])
    window2 = None
    window3 = None

    while True:
        windows, event, values = sg.read_all_windows()
        print(event)
        if event == sg.WIN_CLOSED and windows == window:
            break

        if event == 'Landing Pad Test' and not window2:
            window.hide()
            window2 = landing_pad_commands()

        if event == 'Lid Test' and not window2:
            window.hide()
            window2 = lid_commands()

        if windows == window2 and (event in (sg.WIN_CLOSED, 'Back')):
            window2.close()
            window2 = None
            window.un_hide()

        if event == 'Lid' and windows == window2:
            window2.hide()
            window3 = lid()

        if event == 'Aruco Lights' and windows == window2:
            window2.hide()
            window3 = aruco_lights()

        if event == 'Camera' and windows == window2:
            window2.hide()
            window3 = camera()

        if windows == window3 and (event in (sg.WIN_CLOSED, 'Back')):
            window3.close()
            window3 = None
            window2.un_hide()

        if callable(event):
            event()

        # if event == 'lid_open' or event == 'lid_close' or event == 'lid_stop':
        #     lid_open()

    window.close()


if __name__ == '__main__':
    main()
