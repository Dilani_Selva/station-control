import Tkinter as tk  # for Python 2.x version
import tkFont
import os
# import rospy
# from std_msgs.msg import String

def lid_open_cmd():
    print("lid open")
    os.system("rostopic pub /stationLidCmd std_msgs/String open --once")


def lid_close_cmd():
    print("lid close")
    os.system("rostopic pub /stationLidCmd std_msgs/String close --once")


def lid_stop_cmd():
    print("lid stop")
    os.system("rostopic pub /stationLidCmd std_msgs/String stop --once")


def lid_open_and_close_cmd(count):
    for i in range(count):
        print(i, 'lid opening and closing')
        os.system("rostopic pub /stationLidCmd std_msgs/String open --once")
        os.system("rostopic pub /stationLidCmd std_msgs/String close --once")

def brush_open_cmd():
    print("brush open")
    os.system("rostopic pub /stationBrushCmd std_msgs/String open --once")


def brush_close_cmd():
    print("brush close")
    os.system("rostopic pub /stationBrushCmd std_msgs/String close --once")


def brush_stop_cmd():
    print("brush stop")
    os.system("rostopic pub /stationBrushCmd std_msgs/String stop --once")


def brush_open_and_close_cmd(count):
    for i in range(count):
        print(i, 'brush opening and closing')
        os.system("rostopic pub /stationBrushCmd std_msgs/String open --once")
        os.system("rostopic pub /stationBrushCmd std_msgs/String close --once")


def cameraLightsOn1():
    print('cameraLightsOn1')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn1 --once")


def cameraLightsOn2():
    print('cameraLightsOn2')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn2 --once")


def cameraLightsOn3():
    print('cameraLightsOn3')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn3 --once")


def cameraLightsOn4():
    print('cameraLightsOn4')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn4 --once")


def cameraLightsOff1():
    print('cameraLightsOff1')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff1 --once")


def cameraLightsOff2():
    print('cameraLightsOff2')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff2 --once")


def cameraLightsOff3():
    print('cameraLightsOff3')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff3 --once")


def cameraLightsOff4():
    print('cameraLightsOff4')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff4 --once")


def cameraLightsOff():
    print('cameraLightsOff')
    os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff --once")

# to do


def chargeOn():
    print('chargeOn')
    os.system("rostopic pub /stationCmds std_msgs/String chargeOn --once")


def chargeOff():
    print('chargeOff')
    # os.system("rostopic pub /stationCmds std_msgs/String chargeOff --once")


def droneOn():
    print('droneOn')
    # os.system("rostopic pub /stationCmds std_msgs/String droneOn --once")


def droneOff():
    print('droneOff')
    # os.system("rostopic pub /stationCmds std_msgs/String droneOff --once")


def arucoLightsOn():
    print('arucoLightsOn')
    # os.system("rostopic pub /stationCmds std_msgs/String arucoLightsOn --once")


def arucoLightsOff():
    print('arucoLightsOff')
    # os.system("rostopic pub /stationCmds std_msgs/String arucoLightsOff --once")


def fanOn():
    print('fanOn')
    # os.system("rostopic pub /stationCmds std_msgs/String fanOn --once")


def fanOff():
    print('fanOff')
    # os.system("rostopic pub /stationCmds std_msgs/String fanOff --once")


def buzzer1On():
    print('buzzer1On')
    # os.system("rostopic pub /stationCmds std_msgs/String buzzer1On --once")


def buzzer1Off():
    print('buzzer1Off')
    # os.system("rostopic pub /stationCmds std_msgs/String buzzer1Off --once")


def buzzer2On():
    print('buzzer2On')
    # os.system("rostopic pub /stationCmds std_msgs/String buzzer2On --once")


def buzzer2Off():
    print('buzzer2Off')
    # os.system("rostopic pub /stationCmds std_msgs/String buzzer2Off --once")


def lid_switch_status_cb(data, colour):
    # lid_switch.delete('1.0', tk.END)
    print(colour)

    status_cut = data.data[:2]
    status_split = data.data.split()
    # buttons[0]['text'] = data.data
    # buttons[1]['text'] = data.data
    # buttons[2]['text'] = data.data
    # buttons[3]['text'] = data.data

    if left_open_colour == 'true':
      colour[0] = "green"
    else:
      colour[0] = "red"

    if left_close_colour == 'true':
      colour[0] = "green"
    else:
      colour[0] = "red"

    if lid_switch_right_open == 'true':
      colour[0] = "green"
    else:
      colour[0] = "red"

    if lid_switch_right_close == 'true':
      colour[0] = "green"
    else:
      colour[0] = "red"

def limit_switch_control():
    limit_controls_window = tk.Toplevel(root)
    limit_controls_window.geometry("1920x1080")
    limit_controls_window.title("Limit Switch Controls")

    left_open_colour = "red"
    left_close_colour = "green"
    right_open_colour = "red"
    right_close_colour = "green"

    back = tk.Button(limit_controls_window, text="Back", fg="red", font=FONT, command=limit_controls_window.destroy)
    lid_switch_left_open = tk.Button(limit_controls_window, disabledforeground="black", bg=left_open_colour,text='Left Open', font=FONT, state="disabled")
    lid_switch_left_close = tk.Button(limit_controls_window, disabledforeground="black", bg=left_close_colour, text='Left Close', font=FONT, state="disabled")
    lid_switch_right_open = tk.Button(limit_controls_window, disabledforeground="black", bg=right_open_colour, text='Right Open', font=FONT, state="disabled")
    lid_switch_right_close = tk.Button(limit_controls_window, disabledforeground="black", bg=right_close_colour, text='Right Close', font=FONT, state="disabled")

    buttons = [lid_switch_left_open, lid_switch_left_close, lid_switch_right_open, lid_switch_right_close]
    colour = [left_open_colour, left_close_colour, right_open_colour, right_close_colour]
    # rospy.init_node('build_console', anonymous=True) # Create ros node
    # rospy.Subscriber("stationLidSwitchStatus", String, lid_switch_status_cb, colour)

    limit_controls_window.rowconfigure(
        (1, 2), weight=1)  # make buttons stretch when
    limit_controls_window.columnconfigure(
        (0, 2), weight=1)  # when window is resized

    lid_switch_left_open.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    lid_switch_left_close.grid(row=1, column=1, columnspan=2, sticky='EWNS')
    lid_switch_right_open.grid(row=2, column=0, columnspan=1, sticky='EWNS')
    lid_switch_right_close.grid(row=2, column=1, columnspan=2, sticky='EWNS')
    back.grid(row=0, column=0, columnspan=3, sticky='EWNS')


def motor_status_cb(data):
    print(data.data)


def lid_control():
    lid_controls_window = tk.Toplevel(root)
    lid_controls_window.title("Lid Controls")

    # sets the geometry of toplevel
    lid_controls_window.geometry("1920x1080")

    back = tk.Button(lid_controls_window, text='Back', fg="red",
                     font=FONT, command=lid_controls_window.destroy)
    lid_open = tk.Button(lid_controls_window, text='Open',
                         font=FONT, command=lid_open_cmd)
    lid_close = tk.Button(lid_controls_window, text='Close',
                          font=FONT, command=lid_close_cmd)
    lid_stop = tk.Button(lid_controls_window, text='Stop',
                         font=FONT, command=lid_stop_cmd)
    lid5 = tk.Button(lid_controls_window, text='5', font=FONT,
                     command=lambda: lid_open_and_close_cmd(5))
    lid10 = tk.Button(lid_controls_window, text='10', font=FONT,
                      command=lambda: lid_open_and_close_cmd(10))
    lid20 = tk.Button(lid_controls_window, text='20', font=FONT,
                      command=lambda: lid_open_and_close_cmd(20))
    lid50 = tk.Button(lid_controls_window, text='50', font=FONT,
                      command=lambda: lid_open_and_close_cmd(50))

    lid_controls_window.rowconfigure(
        (0, 1), weight=1)  # make buttons stretch when
    lid_controls_window.columnconfigure(
        (0, 2), weight=1)  # when window is resized

    back.grid(row=0, column=0, columnspan=1, sticky='EWNS')
    lid_open.grid(row=0, column=1, columnspan=1, sticky='EWNS')
    lid_close.grid(row=0, column=2, columnspan=1, sticky='EWNS')
    lid_stop.grid(row=0, column=3, columnspan=1, sticky='EWNS')
    lid5.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    lid10.grid(row=1, column=1, columnspan=1, sticky='EWNS')
    lid20.grid(row=1, column=2, columnspan=1, sticky='EWNS')
    lid50.grid(row=1, column=3, columnspan=1, sticky='EWNS')


def brushes_control():
    brushes_control_window = tk.Toplevel(root)
    brushes_control_window.title("Brushes Controls")

    # sets the geometry of toplevel
    brushes_control_window.geometry("1920x1080")

    back = tk.Button(brushes_control_window, text='Back', fg="red",
                     font=FONT, command=brushes_control_window.destroy)
    brush_open = tk.Button(brushes_control_window,
                           text='Open', font=FONT, command=brush_open_cmd)
    brush_close = tk.Button(brushes_control_window,
                            text='Close', font=FONT, command=brush_close_cmd)
    brush_stop = tk.Button(brushes_control_window,
                           text='Stop', font=FONT, command=brush_stop_cmd)
    lid5 = tk.Button(brushes_control_window, text='5', font=FONT,
                     command=lambda: brush_open_and_close_cmd(5))
    lid10 = tk.Button(brushes_control_window, text='10',
                      font=FONT, command=lambda: brush_open_and_close_cmd(10))
    lid20 = tk.Button(brushes_control_window, text='20',
                      font=FONT, command=lambda: brush_open_and_close_cmd(20))
    lid50 = tk.Button(brushes_control_window, text='50',
                      font=FONT, command=lambda: brush_open_and_close_cmd(50))

    brushes_control_window.rowconfigure(
        (0, 1), weight=1)  # make buttons stretch when
    brushes_control_window.columnconfigure(
        (0, 2), weight=1)  # when window is resized

    back.grid(row=0, column=0, columnspan=1, sticky='EWNS')
    brush_open.grid(row=0, column=1, columnspan=1, sticky='EWNS')
    brush_close.grid(row=0, column=2, columnspan=1, sticky='EWNS')
    brush_stop.grid(row=0, column=3, columnspan=1, sticky='EWNS')
    lid5.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    lid10.grid(row=1, column=1, columnspan=1, sticky='EWNS')
    lid20.grid(row=1, column=2, columnspan=1, sticky='EWNS')
    lid50.grid(row=1, column=3, columnspan=1, sticky='EWNS')


def light_control():
    light_control_window = tk.Toplevel(root)
    light_control_window.title("Light Controls")

    # sets the geometry of toplevel
    light_control_window.geometry("1920x1080")

    back = tk.Button(light_control_window, text='Back', fg="red",
                     font=FONT, command=light_control_window.destroy)
    light1_on = tk.Button(light_control_window, text='1 on',
                          font=FONT, command=cameraLightsOn1)
    light2_on = tk.Button(light_control_window, text='2 on',
                          font=FONT, command=cameraLightsOn2)
    light3_on = tk.Button(light_control_window, text='3 on',
                          font=FONT, command=cameraLightsOn3)
    light4_on = tk.Button(light_control_window, text='4 on',
                          font=FONT, command=cameraLightsOn4)
    off = tk.Button(light_control_window, text='Off',
                    font=FONT, command=cameraLightsOff)
    light1_off = tk.Button(light_control_window, text='1 off',
                           font=FONT, command=cameraLightsOff1)
    light2_off = tk.Button(light_control_window, text='2 off',
                           font=FONT, command=cameraLightsOff2)
    light3_off = tk.Button(light_control_window, text='3 off',
                           font=FONT, command=cameraLightsOff3)
    light4_off = tk.Button(light_control_window, text='4 off',
                           font=FONT, command=cameraLightsOff4)

    light_control_window.rowconfigure(
        (0, 1), weight=1)  # make buttons stretch when
    light_control_window.columnconfigure(
        (0, 2), weight=1)  # when window is resized

    back.grid(row=0, column=0, columnspan=1, sticky='EWNS')
    light1_on.grid(row=0, column=1, columnspan=1, sticky='EWNS')
    light2_on.grid(row=0, column=2, columnspan=1, sticky='EWNS')
    light3_on.grid(row=0, column=3, columnspan=1, sticky='EWNS')
    light4_on.grid(row=0, column=4, columnspan=1, sticky='EWNS')
    off.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    light1_off.grid(row=1, column=1, columnspan=1, sticky='EWNS')
    light2_off.grid(row=1, column=2, columnspan=1, sticky='EWNS')
    light3_off.grid(row=1, column=3, columnspan=1, sticky='EWNS')
    light4_off.grid(row=1, column=4, columnspan=1, sticky='EWNS')


def landing_pad_commands():
    landing_pad_commands_window = tk.Toplevel(root)
    landing_pad_commands_window.title("Landing Pad Commands")
    landing_pad_commands_window.geometry("1920x1080")

    back = tk.Button(landing_pad_commands_window, text='Back', fg="red",
                     font=FONT, command=landing_pad_commands_window.destroy)
    brushes = tk.Button(landing_pad_commands_window,
                        text='Brushes', font=FONT, command=brushes_control)
    charges = tk.Button(landing_pad_commands_window, text='Charges', font=FONT)
    drone_power = tk.Button(landing_pad_commands_window,
                            text='Drone Power', font=FONT)
    motor = tk.Button(landing_pad_commands_window, text='Motor', font=FONT)
    limit_switches = tk.Button(landing_pad_commands_window,
                               text='Limit Switches', font=FONT, command=limit_switch_control)

    landing_pad_commands_window.rowconfigure(
        (0, 1), weight=1)  # make buttons stretch when
    landing_pad_commands_window.columnconfigure(
        (0, 2), weight=1)  # when window is resized

    back.grid(row=0, column=0, columnspan=1, sticky='EWNS')
    brushes.grid(row=0, column=1, columnspan=1, sticky='EWNS')
    charges.grid(row=0, column=2, columnspan=1, sticky='EWNS')
    drone_power.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    motor.grid(row=1, column=1, columnspan=1, sticky='EWNS')
    limit_switches.grid(row=1, column=2, columnspan=1, sticky='EWNS')


def lid_commands():
    lid_commands_window = tk.Toplevel(root)
    lid_commands_window.title("Lid Commands")
    lid_commands_window.geometry("1920x1080")

    back = tk.Button(lid_commands_window, text='Back', fg="red",
                     font=FONT, command=lid_commands_window.destroy)
    lid = tk.Button(lid_commands_window, text='Lid',
                    font=FONT, command=lid_control)
    limit_switches = tk.Button(
        lid_commands_window, text='Limit Switches', font=FONT)
    motor = tk.Button(lid_commands_window, text='Motor', font=FONT)
    aruco_lights = tk.Button(
        lid_commands_window, text='Aruco Lights', font=FONT, command=light_control)
    camera = tk.Button(lid_commands_window, text='Camera', font=FONT)

    lid_commands_window.rowconfigure(
        (0, 1), weight=1)  # make buttons stretch when
    lid_commands_window.columnconfigure(
        (0, 2), weight=1)  # when window is resized

    back.grid(row=0, column=0, columnspan=1, sticky='EWNS')
    lid.grid(row=0, column=1, columnspan=1, sticky='EWNS')
    limit_switches.grid(row=0, column=2, columnspan=1, sticky='EWNS')
    motor.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    aruco_lights.grid(row=1, column=1, columnspan=1, sticky='EWNS')
    camera.grid(row=1, column=2, columnspan=1, sticky='EWNS')


def home_page():
    # tkFont.BOLD == 'bold'

    quit = tk.Button(text='QUIT', fg="red", font=FONT, command=root.destroy)
    full_test = tk.Button(text='Full station test', font=FONT)
    lid = tk.Button(text='Lid', font=FONT, command=lid_commands)
    landing_pad = tk.Button(text='Landing Pad', font=FONT,
                            command=landing_pad_commands)
    station = tk.Button(text='Station', font=FONT)

    root.rowconfigure((0, 1), weight=1)  # make buttons stretch when
    root.columnconfigure((0, 2), weight=1)  # when window is resized
    quit.grid(row=0, column=0, columnspan=1, sticky='EWNS')
    full_test.grid(row=0, column=1, columnspan=2, sticky='EWNS')
    lid.grid(row=1, column=0, columnspan=1, sticky='EWNS')
    landing_pad.grid(row=1, column=1, columnspan=1, sticky='EWNS')
    station.grid(row=1, column=2, columnspan=1, sticky='EWNS')


root = tk.Tk()
root.geometry("1920x1080")
FONT = tkFont.Font(family='Helvetica', size=80, weight=tkFont.BOLD)
FONT2 = tkFont.Font(family='Helvetica', size=20, weight=tkFont.BOLD)
# LID_SWITCH =

# rospy.Subscriber("stationBrushSwitchStatus", String, brush_switch_status_cb) # Create subscriber
# rospy.Subscriber("stationStatusMotorDriver", String, motor_status_cb) # Create subscriber

home_page()

root.mainloop()
