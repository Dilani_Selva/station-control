#!/usr/bin/env python
import sys
import PySimpleGUI as sg
import os
# import rospy
# from std_msgs.msg import String


theme_dict = {'BACKGROUND': '#2B475D',
              'TEXT': '#FFFFFF',
              'INPUT': '#F2EFE8',
              'TEXT_INPUT': '#000000',
              'SCROLL': '#F2EFE8',
              'BUTTON': ('#000000', '#C2D4D8'),
              'PROGRESS': ('#FFFFFF', '#C7D5E0'),
              'BORDER': 1, 'SLIDER_DEPTH': 0, 'PROGRESS_DEPTH': 0}
orange = '#FF8801'

BORDER_COLOR = '#C7D5E0'
DARK_HEADER_COLOR = '#1B2838'
RED = '#FF0000'
GREEN = '#008000'
BPAD_TOP = ((20, 20), (20, 10))
BPAD_LEFT = ((20, 10), (0, 10))
BPAD_LEFT_INSIDE = (0, 10)
BPAD_RIGHT_INSIDE = (20, 20)
BPAD_RIGHT = ((10, 20), (10, 20))
BPAD_FAR_RIGHT = ((0, 30), (20, 30))

LEFT_LID_OPEN = False
LEFT_LID_CLOSE = False


def lid_open():
    print("lid open test")
    # os.system("rostopic pub /stationLidCmd std_msgs/String open --once")


def lid_close():
    print("lid close test")
    # os.system("rostopic pub /stationLidCmd std_msgs/String close --once")


def lid_stop():
    print("lid stop test")
    # os.system("rostopic pub /stationLidCmd std_msgs/String stop --once")


def arucoLightsOn():
    print('arucoLightsOn')
    # os.system("rostopic pub /stationCmds std_msgs/String arucoLightsOn --once")

def arucoLightsOff():
    print('arucoLightsOff')
    # os.system("rostopic pub /stationCmds std_msgs/String arucoLightsOff --once")

def cameraLightsOn1():
  print('cameraLightsOn1')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn1 --once")

def cameraLightsOn2():
  print('cameraLightsOn2')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn2 --once")

def cameraLightsOn3():
  print('cameraLightsOn3')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn3 --once")

def cameraLightsOn4():
  print('cameraLightsOn4')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOn4 --once")

def cameraLightsOff1():
  print('cameraLightsOff1')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff1 --once")

def cameraLightsOff2():
  print('cameraLightsOff2')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff2 --once")

def cameraLightsOff3():
  print('cameraLightsOff3')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff3 --once")

def cameraLightsOff4():
  print('cameraLightsOff4')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff4 --once")

def cameraLightsOff():
  print('cameraLightsOff')
      # os.system("rostopic pub /stationCmds std_msgs/String cameraLightsOff --once")


def brush_open():
    print("brush open")
    # os.system("rostopic pub /stationBrushCmd std_msgs/String open --once")

def brush_close():
    print("brush close")
    # os.system("rostopic pub /stationBrushCmd std_msgs/String close --once")

def brush_stop():
    print("brush stop")
    # os.system("rostopic pub /stationBrushCmd std_msgs/String stop --once")

def chargeOn():
    print('chargeOn')
    # os.system("rostopic pub /stationCmds std_msgs/String chargeOn --once")

def chargeOff():
    print('chargeOff')
    # os.system("rostopic pub /stationCmds std_msgs/String chargeOff --once")

def droneOn():
    print('droneOn')
    # os.system("rostopic pub /stationCmds std_msgs/String droneOn --once")

def droneOff():
    print('droneOff')
    # os.system("rostopic pub /stationCmds std_msgs/String droneOff --once")

def landing_pad_commands():
    windows = generate_windows('Landing Pad Commands', [
                               'Back', 'Brushes', 'Charges', 'Drone Power', 'Limit Switches', 'Motor'])
    return windows

def lid_commands():
    windows = generate_windows('Lid Commands', [
                               'Back', 'Lid', 'Limit Switches', 'Motor', 'Aruco Lights', 'Camera'])
    return windows


def lid():
    return generate_command_windows(
        'Lid',
        ['Left Open', 'Left Close', 'Right Open', 'Right Close'],
        ['Open', 'Close'],
        ['left_lid_open', 'left_lid_close', 'right_lid_open', 'right_lid_close'])

def limit_switches_lid():
    return generate_command_windows(
        'Limit Switches',
        ['Left Open', 'Left Close', 'Right Open', 'Right Close'],
        'On/Off',
        ['left_limit_open', 'left_limit_close', 'right_limit_open', 'right_limit_close'])

def limit_switches_landing_pad():
    return generate_command_windows(
        'Limit Switches',
        ['Left Open', 'Left Close', 'Right Open', 'Right Close'],
        'On/Off',
        ['left_limit_open', 'left_limit_close', 'right_limit_open', 'right_limit_close'])

def motor_lid():
    return generate_command_windows(
        'Motor',
        ['Motor 1 Left', 'Motor 1 Right', 'Motor 2 Left', 'Motor 2 Right'],
        'On/Off',
        ['left_lid_open', 'left_lid_close', 'right_lid_open', 'right_lid_close'])

def motor_landing_pad():
    return generate_command_windows(
        'Motor',
        ['Motor 1 Left', 'Motor 1 Right', 'Motor 2 Left', 'Motor 2 Right'],
        'On/Off',
        ['left_lid_open', 'left_lid_close', 'right_lid_open', 'right_lid_close'])

def aruco_lights():
    print("test")
    return generate_command_windows(
        'Aruco Lights',
        ['Light 1', 'Light 2', 'Light 3', 'Light 4'],
        'On/Off',
        ['light_1', 'light_2', 'light_3', 'light_4'])

def camera():
    return generate_command_windows(
        'Camera',
        ['Camera 1', 'Camera 2', 'Camera 3', 'Camera 4'],
        'On/Off',
        ['camera_1', 'camera_2', 'camera_3', 'camera_4'])

def brushes():
      return generate_command_windows(
        'Brushes',
        ['Left Open', 'Left Close', 'Right Open', 'Right Close'],
        ['Open', 'Close'],
        ['left_brushes_open', 'left_brushes_close', 'right_brushes_open', 'right_brushes_close'])

def charges():
    return generate_command_windows(
        'Charges',
        ['Charges 1', 'Charges 2', 'Charges 3', 'Charges 4'],
        'On/Off',
        ['charges_1', 'charges_2', 'charges_3', 'charges_4'])

def drone_power():
    return generate_command_windows(
        'Drone Power',
        ['Drone 1 On', 'Drone 1 Off', 'Drone 2 On', 'Drone 2 Off'],
        'On/Off',
        ['left_lid_open', 'left_lid_close', 'right_lid_open', 'right_lid_close'])

def cameraLightsOnOff(number):
    for i in range(number):
        cameraLightsOn1()
        cameraLightsOn2()
        cameraLightsOn3()
        cameraLightsOn4()
        cameraLightsOff()
        print(i, 'cameraOn and cameraOff')

def arucoLightsOnOff(number):
    for i in range(number):
        arucoLightsOn()
        arucoLightsOff()
        print(i, 'arucoLightsOn and arucoLightsOff')

def lid_open_and_close(number):
    for i in range(number):
        lid_open()
        lid_close()
        print(i, 'opening and closing')

def generate_command_windows(name, commands, buttons, keys):
    sg.LOOK_AND_FEEL_TABLE[name] = theme_dict
    sg.theme(name)

    top_banner = [[sg.Text(name + ' '*64,
                           font='Any 20', background_color=DARK_HEADER_COLOR)]]
    status = [[sg.Text('<Display Status Here>' + ' '*64,
                       font='Any 20', background_color=DARK_HEADER_COLOR)]]

    if name == 'Lid' or name == 'Brushes':
        left_open = [[sg.Text(commands[0], font='Any 20')],
                    [sg.Button(buttons[0], key=keys[0], font='Any 30', button_color=('white', RED))]]

        left_close = [[sg.Text(commands[1], font='Any 20')],
                      [sg.Button(buttons[1], key=keys[1], font='Any 30', button_color=('white', RED))]]

        right_open = [[sg.Text(commands[2], font='Any 20')],
                      [sg.Button(buttons[0], key=keys[2], font='Any 30', button_color=('white', RED))]]

        right_close = [[sg.Text(commands[3], font='Any 20')],
                      [sg.Button(buttons[1], key=keys[3], font='Any 30', button_color=('white', RED))]]
    else:
        left_open = [[sg.Text(commands[0], font='Any 20')],
                    [sg.Button(buttons, key=keys[0], font='Any 30', button_color=('white', RED))]]

        left_close = [[sg.Text(commands[1], font='Any 20')],
                      [sg.Button(buttons, key=keys[1], font='Any 30', button_color=('white', RED))]]

        right_open = [[sg.Text(commands[2], font='Any 20')],
                      [sg.Button(buttons, key=keys[2], font='Any 30', button_color=('white', RED))]]

        right_close = [[sg.Text(commands[3], font='Any 20')],
                      [sg.Button(buttons, key=keys[3], font='Any 30', button_color=('white', RED))]]

    commands = list_commands('camera')

    logs = [[sg.Text('Logs', font='Any 20')]]

    layout = [[sg.Column(top_banner, size=(1280, 50), pad=(0, 0), background_color=DARK_HEADER_COLOR)],
              [sg.Column(status, size=(1240, 90), pad=BPAD_TOP)],
              [sg.Column([[sg.Column(left_open, size=(220, 120), pad=BPAD_LEFT_INSIDE)],
                          [sg.Column(right_open, size=(220, 120),  pad=BPAD_LEFT_INSIDE)]], pad=BPAD_LEFT, background_color=BORDER_COLOR),
              sg.Column([[sg.Column(left_close, size=(220, 120), pad=BPAD_LEFT_INSIDE)],
                         [sg.Column(right_close, size=(220, 120),  pad=BPAD_LEFT_INSIDE)]], pad=BPAD_RIGHT, background_color=BORDER_COLOR),
              sg.Column(commands, size=(350, 260), pad=BPAD_RIGHT),
              sg.Column(logs, size=(350, 260), pad=BPAD_RIGHT)]]

    return sg.Window(name, layout, size=(1280, 480),
                     margins=(0, 0), background_color=BORDER_COLOR, resizable=True, finalize=True)

def list_commands(command):
    if command == 'lids':
        commands = [[sg.Text('Commands', font='Any 20')],
                    [sg.Button('Open', key=lid_open, font='Any 25'),
                     sg.Button('Close', key=lid_close, font='Any 25'),
                     sg.Button('Stop', key=lid_stop, font='Any 25')],
                    [sg.Button('10', key=lambda: lid_open_and_close(10), font='Any 40'),
                     sg.Button('15', key=lambda: lid_open_and_close(
                         15), font='Any 40'),
                     sg.Button('20', key=lambda: lid_open_and_close(20), font='Any 40')]]

    if command == 'arucoLights':
        commands = [[sg.Text('Commands', font='Any 20')],
                    [sg.Button('On', key=arucoLightsOn, font='Any 35'),
                     sg.Button('Off', key=arucoLightsOff, font='Any 35')],
                    [sg.Button('10', key=lambda: arucoLightsOnOff(10), font='Any 40'),
                     sg.Button('15', key=lambda: arucoLightsOnOff(
                         15), font='Any 40'),
                     sg.Button('20', key=lambda: arucoLightsOnOff(20), font='Any 40')]]

    if command == 'camera':
        commands = [[sg.Text('Commands', font='Any 20')],
                    [sg.Button('On', key=arucoLightsOn, font='Any 35'),
                     sg.Button('Off', key=arucoLightsOff, font='Any 35')],
                    [sg.Button('10', key=lambda: arucoLightsOnOff(10), font='Any 40'),
                     sg.Button('15', key=lambda: arucoLightsOnOff(
                         15), font='Any 40'),
                     sg.Button('20', key=lambda: arucoLightsOnOff(20), font='Any 40')]]

    return commands

def generate_windows(name, buttons):
    sg.LOOK_AND_FEEL_TABLE['Dashboard'] = theme_dict
    sg.theme('Dashboard')

    top_banner = [[sg.Text(name + ' '*64, font='Any 20', background_color=DARK_HEADER_COLOR),
                  sg.Text('Date and Time', font='Any 20', background_color=DARK_HEADER_COLOR)]]

    menu_bar = []
    for i in range(len(buttons)):
        if buttons[i] == 'Back':
            menu_bar.append(
                sg.Button(buttons[i], font='Any 20', button_color=('white', 'firebrick3')))
        else:
            menu_bar.append(sg.Button(buttons[i], font='Any 20'))

    menu_bar = [menu_bar]

    layout = [[sg.Column(top_banner, size=(1280, 50), pad=(0, 0), background_color=DARK_HEADER_COLOR)],
              [sg.Column(menu_bar, size=(1100, 90), pad=BPAD_TOP)]
              ]

    window = sg.Window(name, layout, size=(1280, 480),
                       margins=(0, 0), background_color=BORDER_COLOR, resizable=True, finalize=True)

    return window

def main():

    window = generate_windows('Station Testing App', [
                              'Full Test', 'Walkthrough', 'Landing Pad Test', 'Lid Test'])
    window2 = None
    window3 = None
    prev_window = None

    while True:
        windows, event, values = sg.read_all_windows()
        print(event)
        if event == sg.WIN_CLOSED and windows == window:
            break

        if event == 'Landing Pad Test' and not window2:
            prev_window = 'Landing Pad'
            window.hide()
            window2 = landing_pad_commands()

        if event == 'Lid Test' and not window2:
            prev_window = 'Lid'
            window.hide()
            window2 = lid_commands()

        if windows == window2 and (event in (sg.WIN_CLOSED, 'Back')):
            window2.close()
            window2 = None
            window.un_hide()

        if event == 'Lid' and windows == window2:
            window2.hide()
            window3 = lid()

        if event == 'Limit Switches' and windows == window2 and prev_window == 'Lid':
            window2.hide()
            window3 = limit_switches_lid()

        if event == 'Limit Switches' and windows == window2 and prev_window == 'Landing Pad':
            window2.hide()
            window3 = limit_switches_landing_pad()

        if event == 'Motor' and windows == window2 and prev_window == 'Lid':
            print("prev_window", prev_window, event)
            window2.hide()
            window3 = motor_lid()

        if event == 'Motor' and windows == window2 and prev_window == 'Landing Pad':
            print("prev_window", prev_window, event)
            window2.hide()
            window3 = motor_landing_pad()

        if event == 'Aruco Lights' and windows == window2:
            window2.hide()
            window3 = aruco_lights()

        if event == 'Camera' and windows == window2:
            window2.hide()
            window3 = camera()

        if event == 'Brushes' and windows == window2:
            window2.hide()
            window3 = brushes()

        if event == 'Charges' and windows == window2:
            window2.hide()
            window3 = charges()

        if event == 'Drone Power' and windows == window2:
            window2.hide()
            window3 = drone_power()

        if windows == window3 and (event in (sg.WIN_CLOSED, 'Back')):
            window3.close()
            window3 = None
            window2.un_hide()

        if callable(event):
            event()

        # if event == 'lid_open' or event == 'lid_close' or event == 'lid_stop':
        #     lid_open()

    window.close()


if __name__ == '__main__':
    main()
