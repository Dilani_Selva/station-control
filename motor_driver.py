#!/usr/bin/env python

# Code prepared by Robin Gojon for Herotech8 Ltd
# Contact details robin.gojon@herotech8.com or contact@herotech8.com

# This code is acting on a TLE94112EL : motor control board connected over SPI to a Raspberry Pi
# The motors are used to actuate the lid of the Herotech8 drone station

# import library
# use of spidev library for SPI read/write operations
# Library information can be found here: https://github.com/doceme/py-spidev
import time
import rospy
from std_msgs.msg import String
from std_msgs.msg import Bool
import smbus
import spidev

# Use this library for GPIO extender over I2C control: https://www.raspberrypi-spy.co.uk/2013/07/how-to-use-a-mcp23017-i2c-port-expander-with-the-raspberry-pi-part-2/

# Prepare I2C
bus = smbus.SMBus(1)

# Set I2C registers
DEVICE = 0x21 # Device address (A0-A2)
IODIRB = 0x01 # Pin direction register
OLATB  = 0x15 # Register for outputs
GPIOB  = 0x13 # Register for inputs
GPUUB = 0x0D # Register for pull up port B

# Switches
BRUSH_CHARGE_OPEN_MASK   = 0b00010000
BRUSH_CHARGE_CLOSED_MASK   = 0b00100000
BRUSH_SIDE_OPEN_MASK   = 0b01000000
BRUSH_SIDE_CLOSED_MASK   = 0b10000000
LID_LEFT_OPEN_MASK   = 0b00000001
LID_LEFT_CLOSED_MASK   = 0b00000010
LID_RIGHT_OPEN_MASK   = 0b00001000
LID_RIGHT_CLOSED_MASK   = 0b00000100

# Set all GPIO B pins as intputs by setting
# all bits of IODIRB register to 1
bus.write_byte_data(DEVICE, IODIRB, 0xFF)

# Activate pull up resistors on input port B
bus.write_byte_data(DEVICE, GPUUB, 0xFF)


# The following information comes from the device datasheet: https://www.infineon.com/dgdl/Infineon-TLE94112EL-DS-v01_00-EN.pdf?fileId=5546d462576f347501579a2795837d3e

# Addresses of the control registers
# Addresses are 7-bit long
HB_ACT_1_CTRL = 0b0000011     #Activation/deactivation of  HB1-4
HB_ACT_2_CTRL = 0b1000011    # Activation/deactivation of  HB5-8
HB_ACT_3_CTRL = 0b0100011    # Activation/deactivation of  HB9-12
HB_MODE_1_CTRL = 0b1100011   # Control mode (PWM enable, PWM channel) of HB1-4
HB_MODE_2_CTRL = 0b0010011   # Control mode (PWM enable, PWM channel) of HB5-8
HB_MODE_3_CTRL = 0b1010011   # Control mode (PWM enable, PWM channel) of HB9-12
PWM_CH_FREQ_CTRL = 0b0110011 # PWM clock frequency and frequency modulation of the oscillator
PWM1_DC_CTRL = 0b1110011    # Duty cycle of PWM Channel 1
PWM2_DC_CTRL = 0b0001011     # Duty cycle of PWM Channel 2
PWM3_DC_CTRL = 0b1001011     # Duty cycle of PWM Channel 3
FW_OL_CTRL = 0b0101011       # LED mode for HS1/2 and active free-wheeling of HB1-6
FW_CTRL = 0b1101011          # Active free-wheeling of HB7-12
CONFIG_CTRL = 0b1100111      # Device ID

# Address of the status registers
# Addresses are 7-bit long
SYS_DIAG_1 = 0b0011011      # Global Status Register
SYS_DIAG_2 = 0b1011011      # Overcurrent HB1-4
SYS_DIAG_3 = 0b0111011      # Overcurrent HB5-8
SYS_DIAG_4 = 0b1111011      # Overcurrent HB9-12
SYS_DIAG_5 = 0b0000111      # Open load HB1-4
SYS_DIAG_6 = 0b1000111      # Open load HB5-8
SYS_DIAG_7 = 0b0100111      # Open load HB9-12


# Masks
WRITE = 0b10000000 # Mask for write commands to control registers
READ = 0b00000000 # Mask for read commands to control registers
CLEAR = 0b10000000 # Mask for clear commands to status registers
OC_LID_LEFT_OPEN_MASK       = 0b00000001
OC_LID_RIGHT_OPEN_MASK      = 0b01000000
OC_LID_LEFT_CLOSED_MASK     = 0b00000010
OC_LID_RIGHT_CLOSED_MASK    = 0b10000000
TEMP_SHUTDOWN_MASK          = 0b00000100
TEMP_PRE_WARNING_MASK       = 0b00000010
OVER_VOLTAGE_MASK           = 0b00010000
OPEN_LOAD_OR_OVERCURRENT_MASK   = 0b01000000



# Create SPI object
spi = spidev.SpiDev()

lid_switch_left_open = 0
lid_switch_left_closed = 0
lid_switch_right_open = 0
lid_switch_right_closed = 0

charge_brush_switch_open = 0
charge_brush_switch_closed = 0
side_brush_switch_open = 0
side_brush_switch_closed = 0

lid_command = 'stop'
lid_status = 'stop'

brush_command = 'stop'
brush_status = 'stop'

lid_timer = time.time()
brush_timer = time.time()
status_led_timer = time.time()
status_led_blink_status = True

brush_new_command_flag = False
# global publisher
pub_status_motor_driver = rospy.Publisher('stationStatusMotorDriver', String, queue_size=10) # Create publisher
pub_status_lid_switches = rospy.Publisher('stationLidSwitchStatus', String, queue_size=10) # Create publisher
pub_status_brush_switches = rospy.Publisher('stationBrushSwitchStatus', String, queue_size=10) # Create publisher

# Code for new motors
# Set I2C registers
DEVICE2 = 0x27 # Device address (A0-A2)
IODIRB = 0x01 # Pin direction register
OLATB  = 0x15 # Register for outputs

MOTOR_LID_LEFT_CLOSED_MASK = 0b00010000
MOTOR_LID_LEFT_OPEN_MASK = 0b00100000
MOTOR_LID_RIGHT_CLOSED_MASK = 0b01000000
MOTOR_LID_RIGHT_OPEN_MASK = 0b10000000
MOTOR_BRUSH_CHARGE_CLOSED_MASK = 0b00000010
MOTOR_BRUSH_CHARGE_OPEN_MASK = 0b00000001
MOTOR_BRUSH_SIDE_CLOSED_MASK = 0b0000100
MOTOR_BRUSH_SIDE_OPEN_MASK = 0b00001000

# Set all GPA pins as outputs by setting all bits of IODIRA register to 0
bus.write_byte_data(DEVICE2, IODIRB, 0x00)
# Set output all 8 output bits to 0 to stop the motors
bus.write_byte_data(DEVICE2, OLATB, 0b00000000)


def spi_setup():
    # SPI bus setup
    bus = 0  # SPI bus is 0
    device = 0  # Chip select pin is 0

    spi.open(bus, device)  # Open a connection to a specific bus and device (chip select pin)
    spi.max_speed_hz = 1000000  # Set SPI speed and mode
    spi.mode = 0b01  # Set SPI mode Polarity and Phase in accoradance with datasheet
    # spi.lsbfirst = True # This command fails. This is why I am inverting the bit order manually in the writeRegister and readRegister functions
    # spi.cshigh = False # Seems to be CS low by default


# This function sends the "data" to the "address"
# Input: address in binary format
# Input: data in binary format
def write_register(address, data):

    #print "To write - raw address from datasheet " + bin(address)

    # modify address to include the write bit
    address_with_write_bit = address | WRITE
    #print "Address with write bit " + bin(address_with_write_bit)

    # inverted address with write bit
    inverted_address_with_write_bit = int('{:08b}'.format(address_with_write_bit)[::-1], 2)
    #print "Inverted_address_with_write_bit " + bin(inverted_address_with_write_bit)

    # invert data as well
    inverted_data = int('{:08b}'.format(data)[::-1], 2)

    received_bytes = spi.xfer2([inverted_address_with_write_bit, inverted_data])

    #print "First byte received - Global Status Register " + bin(receivedBytes[0])
    #print "Second byte received - Value of the register we wrote to " + bin(receivedBytes[1])


# This function reads the content of the "address"
# Input: address to read in binary format
def read_register(address):

    #print "To read - raw address from datasheet " + bin(address)

    # modify address to include the read bit
    address_with_read_bit = address | READ
    #print "Address with read bit " + bin(address_with_read_bit)

    # inverted address with read bit
    inverted_address_with_read_bit = int('{:08b}'.format(address_with_read_bit)[::-1], 2)
    #print "Inverted_address_with_read_bit " + bin(inverted_address_with_read_bit)

    received_bytes = spi.xfer2([inverted_address_with_read_bit,0x00])

    received_byte_0 = received_bytes[0]
    received_byte_1 = received_bytes[1]

    inverted_received_byte_0 = int('{:08b}'.format(received_byte_0)[::-1], 2)
    inverted_received_byte_1 = int('{:08b}'.format(received_byte_1)[::-1], 2)

    #print "First byte received - Global Status Register" + bin(inverted_received_byte_0)
    #print "Second byte received - Value of the register we read from " + bin(inverted_received_byte_1)

    return inverted_received_byte_1


def stop_lid_motor_left():
    write_register(HB_ACT_2_CTRL, read_register(HB_ACT_2_CTRL) & 0b11110000)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) & ~MOTOR_LID_LEFT_CLOSED_MASK)
    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) & ~MOTOR_LID_LEFT_OPEN_MASK)

def stop_lid_motor_right():
    write_register(HB_ACT_2_CTRL, read_register(HB_ACT_2_CTRL) & 0b00001111)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) & ~MOTOR_LID_RIGHT_CLOSED_MASK)
    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) & ~MOTOR_LID_RIGHT_OPEN_MASK)


def open_lid_motor_left():
    write_register(HB_ACT_2_CTRL, read_register(HB_ACT_2_CTRL) & 0b11110000 | 0b00001001)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) | MOTOR_LID_LEFT_OPEN_MASK)


def open_lid_motor_right():
    write_register(HB_ACT_2_CTRL, read_register(HB_ACT_2_CTRL) & 0b00001111 | 0b01100000)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) | MOTOR_LID_RIGHT_OPEN_MASK)


def close_lid_motor_left():
    write_register(HB_ACT_2_CTRL, read_register(HB_ACT_2_CTRL) & 0b11110000 | 0b00000110)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) | MOTOR_LID_LEFT_CLOSED_MASK)


def close_lid_motor_right():
    write_register(HB_ACT_2_CTRL, read_register(HB_ACT_2_CTRL) & 0b00001111 | 0b10010000)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) | MOTOR_LID_RIGHT_CLOSED_MASK)


def stop_charge_brush_motor():
    write_register(HB_ACT_1_CTRL, read_register(HB_ACT_1_CTRL) & 0b11110000)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) & ~MOTOR_BRUSH_CHARGE_CLOSED_MASK)
    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) & ~MOTOR_BRUSH_CHARGE_OPEN_MASK)


def stop_side_brush_motor():
    write_register(HB_ACT_1_CTRL, read_register(HB_ACT_1_CTRL) & 0b00001111)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) & ~MOTOR_BRUSH_SIDE_CLOSED_MASK)
    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) & ~MOTOR_BRUSH_SIDE_OPEN_MASK)


def open_charge_brush_motor():
    write_register(HB_ACT_1_CTRL, read_register(HB_ACT_1_CTRL) & 0b11110000 | 0b00001001)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) | MOTOR_BRUSH_CHARGE_OPEN_MASK)


def open_side_brush_motor():
    write_register(HB_ACT_1_CTRL, read_register(HB_ACT_1_CTRL) & 0b00001111 | 0b01100000)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) | MOTOR_BRUSH_SIDE_OPEN_MASK)


def close_charge_brush_motor():
    write_register(HB_ACT_1_CTRL, read_register(HB_ACT_1_CTRL) & 0b11110000 | 0b00000110)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) | MOTOR_BRUSH_CHARGE_CLOSED_MASK)


def close_side_brush_motor():
    write_register(HB_ACT_1_CTRL, read_register(HB_ACT_1_CTRL) & 0b00001111 | 0b10010000)

    bus.write_byte_data(DEVICE2, OLATB, bus.read_byte_data(DEVICE2, OLATB) | MOTOR_BRUSH_SIDE_CLOSED_MASK)


# Stop all motors with a simple function
def stop_motors():
    write_register(HB_ACT_1_CTRL, 0b00000000) # open half bridges 1-4
    write_register(HB_ACT_2_CTRL, 0b00000000)  # open half bridges 5-8

    stop_lid_motor_left()
    stop_lid_motor_right()

    stop_side_brush_motor()
    stop_charge_brush_motor()

# Clear all error flags with a simple function
def clear_flags():
    #print "clear_flags"
    write_register(SYS_DIAG_1, 0x00) # reset global status
    write_register(SYS_DIAG_2, 0x00) # reset over current half bridges 1-4
    write_register(SYS_DIAG_3, 0x00) # reset over current half bridges 5-8
    write_register(SYS_DIAG_5, 0x00) # reset open load half bridges 1-4
    write_register(SYS_DIAG_6, 0x00) # reset open load half bridges 5-8


# global status
def read_sys_diag_1():
    return read_register(SYS_DIAG_1)


# over current half bridges 1-4
def read_sys_diag_2():
    return read_register(SYS_DIAG_2)


# over current half bridges 5-8
def read_sys_diag_3():
    return read_register(SYS_DIAG_3)


# open load half bridges 1-4
def read_sys_diag_5():
    return read_register(SYS_DIAG_5)


# open load half bridges 5-8
def read_sys_diag_6():
    return read_register(SYS_DIAG_6)


def verify_and_clear_diag_registers():

    clear_flag_action = False

    sys_diag_1 = read_sys_diag_1()
    if sys_diag_1 != 0:
        # print "general status - sys_diag_1: " + str(bin(sys_diag_1))
        if sys_diag_1 & TEMP_SHUTDOWN_MASK  > 0:
            pub_status_motor_driver.publish("%s - Temperature shutdown - Stop all motors" % rospy.get_time())
            stop_lid_motor_left()
            stop_lid_motor_right()
            stop_side_brush_motor()
            stop_charge_brush_motor()

        if sys_diag_1 & TEMP_PRE_WARNING_MASK > 0:
            pub_status_motor_driver.publish("%s - Temperature pre warning - Stop all motors" % rospy.get_time())
            stop_lid_motor_left()
            stop_lid_motor_right()
            stop_side_brush_motor()
            stop_charge_brush_motor()

        if sys_diag_1 & OVER_VOLTAGE_MASK > 0:
            pub_status_motor_driver.publish("%s - Overvoltage - Stop all motors" % rospy.get_time())
            stop_lid_motor_left()
            stop_lid_motor_right()
            stop_side_brush_motor()
            stop_charge_brush_motor()

        clear_flag_action = True

    sys_diag_2 = read_sys_diag_2()
    if sys_diag_2 != 0:
        # Brush overcurrent register
        pub_status_motor_driver.publish("%s - Brush overcurrent - diag_2: %s" % (rospy.get_time(), str(bin(sys_diag_2))))
        clear_flag_action = True

    sys_diag_3 = read_sys_diag_3()
    if sys_diag_3 != 0:
        # Lid overcurrent register
        pub_status_motor_driver.publish("%s - Lid overcurrent - diag_3: %s" % (rospy.get_time(), str(bin(sys_diag_3))))
        clear_flag_action = True

    sys_diag_5 = read_sys_diag_5()
    if sys_diag_5 != 0:
        # Brush Open Load register
        pub_status_motor_driver.publish("%s - Brush open load - diag_5: %s" % (rospy.get_time(), str(bin(sys_diag_5))))
        clear_flag_action = True

    sys_diag_6 = read_sys_diag_6()
    if sys_diag_6 != 0:
        # Lid Open Load register
        pub_status_motor_driver.publish("%s - Lid open load - diag_6: %s" % (rospy.get_time(),str(bin(sys_diag_6))))
        clear_flag_action = True

    if clear_flag_action == True:
        clear_flags()


def read_brush_switches():
    # value is 0 when not clicked and 1 when clicked
    global charge_brush_switch_open, charge_brush_switch_closed, side_brush_switch_open, side_brush_switch_closed
    charge_brush_switch_open = bus.read_byte_data(DEVICE, GPIOB) & BRUSH_CHARGE_OPEN_MASK > 0
    charge_brush_switch_closed = bus.read_byte_data(DEVICE, GPIOB) & BRUSH_CHARGE_CLOSED_MASK > 0
    side_brush_switch_open = bus.read_byte_data(DEVICE, GPIOB) & BRUSH_SIDE_OPEN_MASK > 0
    side_brush_switch_closed = bus.read_byte_data(DEVICE, GPIOB) & BRUSH_SIDE_CLOSED_MASK > 0

    pub_status_brush_switches.publish("brush switch - charge_open: " + str(charge_brush_switch_open) + "   charge_closed: " + str(charge_brush_switch_closed) + "   side_open: " + str(side_brush_switch_open) + "   side_closed: " + str(side_brush_switch_closed))


def read_lid_switches():
    # value used to be 1 when not clicked and 0 when clicked
    # with the 'not' it is now 0 when not clicked and 1 when clicked
    global lid_switch_left_open, lid_switch_left_closed, lid_switch_right_open, lid_switch_right_closed
    lid_switch_left_open = not (bus.read_byte_data(DEVICE, GPIOB) & LID_LEFT_OPEN_MASK > 0)
    lid_switch_left_closed = not (bus.read_byte_data(DEVICE, GPIOB) & LID_LEFT_CLOSED_MASK > 0)
    lid_switch_right_open = not (bus.read_byte_data(DEVICE, GPIOB) & LID_RIGHT_OPEN_MASK > 0)
    lid_switch_right_closed = not (bus.read_byte_data(DEVICE, GPIOB) & LID_RIGHT_CLOSED_MASK > 0)

    pub_status_lid_switches.publish("lid switch - left_open: " + str(lid_switch_left_open) + "   left_closed: " + str(lid_switch_left_closed) + "   right_open: " + str(lid_switch_right_open) + "   right_closed: " + str(lid_switch_right_closed))



def manage_lid():
    global lid_status, lid_command, lid_timer, lid_switch_left_open, lid_switch_left_closed, lid_switch_right_open, lid_switch_right_closed

    # timer checks for lid status
    if time.time() - lid_timer > 12:

        stop_lid_motor_left()
        stop_lid_motor_right()

        if lid_status == 'closing':
            lid_status = 'closed'
            if lid_switch_left_closed == False:
                pub_status_motor_driver.publish("%s - Left Lid slow on closing or switches not clicking" % rospy.get_time())
            if lid_switch_right_closed == False:
                pub_status_motor_driver.publish("%s - Right Lid slow on closing or switches not clicking" % rospy.get_time())
        elif lid_status == 'opening':
            lid_status = 'opened'
            if lid_switch_left_open == False:
                pub_status_motor_driver.publish("%s - Left lid slow on opening or switches not clicking" % rospy.get_time())
            if lid_switch_right_open == False:
                pub_status_motor_driver.publish("%s - Right lid slow on opening or switches not clicking" % rospy.get_time())

    # switch checks for lid status
    read_lid_switches()

    if lid_command == 'open':

        stop_lid_motor_left()
        stop_lid_motor_right()

        if lid_switch_left_open == True:
            # do nothing
            stop_lid_motor_left()
        else:
            # open lid
            open_lid_motor_left()

        if lid_switch_right_open == True:
            # do nothing
            stop_lid_motor_right()
        else:
            # open lid
            open_lid_motor_right()

        # do not run this twice
        lid_command = 'opening'
        lid_status = 'opening'

    if lid_status == 'opening' and lid_switch_left_open == True:
        stop_lid_motor_left()

    if lid_status == 'opening' and lid_switch_right_open == True:
        stop_lid_motor_right()

    if lid_status == 'opening' and lid_switch_left_open == True and lid_switch_right_open == True:
        stop_lid_motor_left()
        stop_lid_motor_right()
        lid_status = 'opened'



    if lid_command == 'close':

        stop_lid_motor_left()
        stop_lid_motor_right()

        if lid_switch_left_closed == True:
            # do nothing
            stop_lid_motor_left()
        else:
            # open lid
            close_lid_motor_left()

        if lid_switch_right_closed == True:
            # do nothing
            stop_lid_motor_right()
        else:
            # open lid
            close_lid_motor_right()

        # do not run this twice
        lid_command = 'closing'
        lid_status = 'closing'


    if lid_status == 'closing' and lid_switch_left_closed == True:
        stop_lid_motor_left()

    if lid_status == 'closing' and lid_switch_right_closed == True:
        stop_lid_motor_right()

    if lid_status == 'closing' and lid_switch_left_closed == True and lid_switch_right_closed == True:
        stop_lid_motor_left()
        stop_lid_motor_right()
        lid_status = 'closed'



    if lid_command == 'stop':
        lid_status = 'stop'
        stop_lid_motor_left()
        stop_lid_motor_right()



    # ignore overcurrent below .5 s after command received - else stop motors
    sys_diag_3 = read_sys_diag_3()
    if sys_diag_3 != 0:

        if time.time() - lid_timer < 1.5:
            pub_status_motor_driver.publish("%s - Lid overcurrent within 0.5 second after command is received - ignored" % rospy.get_time())

        else:
            if sys_diag_3 & 0b100 > 0:
                pub_status_motor_driver.publish("%s - Overcurrent protection - stop lid left closing" % rospy.get_time())
                stop_lid_motor_left()
            if sys_diag_3 & 0b10000 > 0:
                pub_status_motor_driver.publish("%s - Overcurrent protection - stop lid right closing" % rospy.get_time())
                stop_lid_motor_right()
            if sys_diag_3 & 0b1000000 > 0:
                pub_status_motor_driver.publish("%s - Overcurrent protection - stop lid right opening" % rospy.get_time())
                stop_lid_motor_right()
            if sys_diag_3 & 0b1 > 0:
                pub_status_motor_driver.publish("%s - Overcurrent protection - stop lid left opening" % rospy.get_time())
                stop_lid_motor_left()


def manage_brush():
    global brush_status, brush_command, brush_new_command_flag, brush_timer, side_brush_switch_open, side_brush_switch_closed, charge_brush_switch_open, charge_brush_switch_closed

    # timer checks for brush status
    if time.time() - brush_timer > 12:
        stop_side_brush_motor()
        stop_charge_brush_motor()

        if brush_status == 'closing':
            brush_status = 'closed'
            if side_brush_switch_closed == False:
                pub_status_motor_driver.publish("%s - Side brush slow on closing or switches not clicking" % rospy.get_time())
            elif charge_brush_switch_closed == False:
                pub_status_motor_driver.publish("%s - Charge brush slow on closing or closed switches not clicking" % rospy.get_time())

        elif brush_status == 'opening':
            brush_status = 'opened'
            if side_brush_switch_open == False:
                pub_status_motor_driver.publish("%s - Side brush slow on opening or switches not clicking" % rospy.get_time())
            if charge_brush_switch_open == False:
                pub_status_motor_driver.publish("%s - Charge brush slow on opening or switches not clicking" % rospy.get_time())





    # switch checks for brush status

    read_brush_switches()

    if brush_command == 'open':

        stop_side_brush_motor()
        stop_charge_brush_motor()

        if side_brush_switch_open == True:
            # do nothing
            stop_side_brush_motor()
        else:
            # open side brush
            open_side_brush_motor()

        if charge_brush_switch_open == True:
            # do nothing
            stop_charge_brush_motor()
        else:
            # open charge brush
            open_charge_brush_motor()

        # do not run this twice
        brush_command = 'opening'
        brush_status = 'opening'

    if brush_status == 'opening' and side_brush_switch_open == True:
        stop_side_brush_motor()

    if brush_status == 'opening' and charge_brush_switch_open == True:
        stop_charge_brush_motor()

    if brush_status == 'opening' and side_brush_switch_open == True and charge_brush_switch_open == True:
        brush_status = 'opened'
        stop_side_brush_motor()
        stop_charge_brush_motor()


    if brush_command == 'close':

        stop_side_brush_motor()
        stop_charge_brush_motor()

        if side_brush_switch_closed == True:
            # do nothing
            stop_side_brush_motor()
        else:
            # close side brush
            close_side_brush_motor()

        # do not run this twice
        brush_new_command_flag = True
        brush_command = 'closing'
        brush_status = 'closing'

    # manage the closing
    if brush_status == 'closing' and side_brush_switch_closed == True and charge_brush_switch_closed == False and brush_new_command_flag == True:
        stop_side_brush_motor()
        brush_timer = time.time()
        close_charge_brush_motor()
        brush_new_command_flag = False

    if brush_status == 'closing' and side_brush_switch_closed == True:
        stop_side_brush_motor()

    if brush_status == 'closing' and charge_brush_switch_closed == True:
        stop_charge_brush_motor()

    if brush_status == 'closing' and side_brush_switch_closed == True and charge_brush_switch_closed == True:
        brush_status = 'closed'
        stop_side_brush_motor()
        stop_charge_brush_motor()



    if brush_command == 'stop':
        brush_status = 'stop'
        stop_side_brush_motor()
        stop_charge_brush_motor()



    # ignore overcurrent below .5 s after command received - else stop motors
    sys_diag_2 = read_sys_diag_2()
    if sys_diag_2 != 0:

        if time.time() - brush_timer < 1.5:
            pub_status_motor_driver.publish("%s - Brush overcurrent within 0.5 second after command is received - ignored" % rospy.get_time())

        else:
            if sys_diag_2 & 0b100 > 0:
                pub_status_motor_driver.publish("%s - Overcurrent protection - stop charge brush closing" % rospy.get_time())
                stop_charge_brush_motor()
            if sys_diag_2 & 0b10000000 > 0:
                pub_status_motor_driver.publish("%s - Overcurrent protection - stop side brush closing" % rospy.get_time())
                stop_side_brush_motor()
            if sys_diag_2 & 0b1000000 > 0:
                pub_status_motor_driver.publish("%s - Overcurrent protection - stop side brush opening" % rospy.get_time())
                stop_side_brush_motor()
            if sys_diag_2 & 0b1 > 0:
                pub_status_motor_driver.publish("%s - Overcurrent protection - stop charge brush opening" % rospy.get_time())
                stop_charge_brush_motor()


def manage_motors():

    manage_lid()
    manage_brush()
    verify_and_clear_diag_registers()



def station_lid_cmd_cb(data):

    rospy.loginfo(rospy.get_caller_id() + "station lid command %s", data.data)

    global lid_timer
    lid_timer = time.time()

    global lid_command

    if data.data == 'open':
        lid_command = 'open'
        pub_status_motor_driver.publish("%s - Receive lid open command" % rospy.get_time())

    elif data.data == 'close' or data.data == 'closed':
        lid_command = 'close'
        pub_status_motor_driver.publish("%s - Receive lid close command" % rospy.get_time())

    elif data.data == 'stop':
        lid_command = 'stop'
        pub_status_motor_driver.publish("%s - Receive lid stop command" % rospy.get_time())


def station_brush_cmd_cb(data):

    rospy.loginfo(rospy.get_caller_id() + "station brush command %s", data.data)

    global brush_timer
    brush_timer = time.time()

    global brush_command
    if data.data == 'open':
        brush_command = 'open'
        pub_status_motor_driver.publish("%s - Receive brush open command" % rospy.get_time())

    elif data.data == 'close' or data.data == 'closed':
        brush_command = 'close'
        pub_status_motor_driver.publish("%s - Receive brush close command" % rospy.get_time())

    elif data.data == 'stop':
        brush_command = 'stop'
        pub_status_motor_driver.publish("%s - Receive brush stop command" % rospy.get_time())


def station_lid_motor_cmd_cb(data):

    rospy.loginfo(rospy.get_caller_id() + "station lid motor command %s", data.data)

    global lid_timer
    lid_timer = time.time()

    if data.data == 'leftOpen':
        open_lid_motor_left()
        pub_status_motor_driver.publish("%s - Receive left lid motor open command" % rospy.get_time())

    elif data.data == 'leftClose':
        close_lid_motor_left()
        pub_status_motor_driver.publish("%s - Receive left lid motor close command" % rospy.get_time())

    elif data.data == 'leftStop':
        stop_lid_motor_left()
        pub_status_motor_driver.publish("%s - Receive left lid motor stop command" % rospy.get_time())
    elif data.data == 'rightOpen':
        open_lid_motor_right()
        pub_status_motor_driver.publish("%s - Receive right lid motor open command" % rospy.get_time())

    elif data.data == 'rightClose':
        close_lid_motor_right()
        pub_status_motor_driver.publish("%s - Receive right lid motor close command" % rospy.get_time())

    elif data.data == 'rightStop':
        stop_lid_motor_right()
        pub_status_motor_driver.publish("%s - Receive right lid motor stop command" % rospy.get_time())


def station_brush_motor_cmd_cb(data):
  
    rospy.loginfo(rospy.get_caller_id() + "station brush motor command %s", data.data)

    global brush_timer
    brush_timer = time.time()

    if data.data == 'chargeOpen':
        open_charge_brush_motor()
        pub_status_motor_driver.publish("%s - Receive charge motor open command" % rospy.get_time())

    elif data.data == 'chargeClose':
        close_charge_brush_motor()
        pub_status_motor_driver.publish("%s - Receive charge motor close command" % rospy.get_time())

    elif data.data == 'chargeStop':
        stop_charge_brush_motor()
        pub_status_motor_driver.publish("%s - Receive charge motor stop command" % rospy.get_time())

    if data.data == 'sideOpen':
        open_side_brush_motor()
        pub_status_motor_driver.publish("%s - Receive side motor open command" % rospy.get_time())

    elif data.data == 'sideClose':
        close_side_brush_motor()
        pub_status_motor_driver.publish("%s - Receive side motor close command" % rospy.get_time())

    elif data.data == 'sideStop':
        stop_side_brush_motor()
        pub_status_motor_driver.publish("%s - Receive side motor stop command" % rospy.get_time())

def main_loop():
    # ROS Setup
    rospy.init_node('station_lid_and_brushes', anonymous=True) # Create ros node
    pub_lid_status = rospy.Publisher('stationLidStatus', String, queue_size=10) # Create publisher
    pub_brush_status = rospy.Publisher('stationBrushStatus', String, queue_size=10) # Create publisher
    pub_lid_open = rospy.Publisher('stationLidOpen', Bool, queue_size=10) # Create publisher
    pub_status_led = rospy.Publisher('stationCmds', String, queue_size=10) # Create publisher
    rospy.Subscriber("stationLidCmd", String, station_lid_cmd_cb)  # Create subscriber
    rospy.Subscriber("stationBrushCmd", String, station_brush_cmd_cb) # Create subscriber
    rospy.Subscriber("stationLidMotorCmd", String, station_lid_motor_cmd_cb)
    rospy.Subscriber("stationBrushMotorCmd", String, station_brush_motor_cmd_cb)

    rate = rospy.Rate(10) # Set loop frequency to 10hz

    spi_setup()

    # give the system some time to initialise
    time.sleep(2)

    stop_motors() # duty cycle to 0 and open the half bridges

    while not rospy.is_shutdown():
        pub_lid_status.publish(lid_status.upper())
        pub_brush_status.publish(brush_status.upper())

        if lid_status == 'opened':
            pub_lid_open.publish(True)
        else:
            pub_lid_open.publish(False)

        manage_motors()

        global status_led_timer, status_led_blink_status


        if lid_status == 'opening' or lid_status == 'closing':
            if time.time() - status_led_timer > 0.25:
                if status_led_blink_status == True:
                    pub_status_led.publish("statusLight3Amber")
                    status_led_blink_status = False
                elif status_led_blink_status == False:
                    pub_status_led.publish("statusLight3Green")
                    status_led_blink_status = True
                status_led_timer = time.time()


        if lid_status == 'opened' or lid_status == 'closed' or lid_status == 'stop':
            if time.time() - status_led_timer > 1:
                if status_led_blink_status == True:
                    pub_status_led.publish("statusLight3Amber")
                    status_led_blink_status = False
                elif status_led_blink_status == False:
                    pub_status_led.publish("statusLight3Green")
                    status_led_blink_status = True
                status_led_timer = time.time()


        rate.sleep()

if __name__ == '__main__':
    try:
        main_loop()
    except rospy.ROSInterruptException:
        spi.close()
        pass